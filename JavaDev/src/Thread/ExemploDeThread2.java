package Thread;

import javax.swing.JOptionPane;

public class ExemploDeThread2 {
	private int i = 0;
	private Thread t = new Thread() {
		public void run() {
			for (;i < 10;i++) {
				System.out.println("oi!");
			}
		}
	};
	
	public static void main(String[] args) {		
		ExemploDeThread2 ex =new ExemploDeThread2();
		ex.t.start();
		String nome = JOptionPane.showInputDialog("informe seu nome");
		JOptionPane.showMessageDialog(null, "Bem vindo, " + nome);
	}
	
	
}
