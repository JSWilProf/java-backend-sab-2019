package Thread.resposta.ballExpress;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Thread.ball.Ball;


@SuppressWarnings("serial")
class BounceExpress extends JFrame {
  private JPanel canvas = new JPanel();
  private int cnt = 0;
  private JLabel contador = new JLabel(Integer.toString(cnt));
  private ThreadGroup tg = new ThreadGroup("bolas");

  public BounceExpress() {
    setTitle("Bounce");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Container contentPane = getContentPane();
    canvas.setPreferredSize(new Dimension(350, 200));
    contentPane.add(canvas, "Center");

    JPanel temp = new JPanel();
    JButton btStart = new JButton("Iniciar");
    btStart.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Thread t1 = new Thread(tg, new ExpressBall(canvas, Color.black));
        t1.setPriority(Thread.MIN_PRIORITY);

        ExpressBall br = new ExpressBall(canvas, Color.red);
        br.setX(4);
        br.setY(4);
        Thread t2 = new Thread(tg, br);
        t2.setPriority(Thread.MAX_PRIORITY);
        
        t1.start();
        t2.start();

        cnt += 2;
        contador.setText(Integer.toString(cnt));
      }
    });
    temp.add(btStart);
    JButton btStop = new JButton("Parar");
    btStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        tg.interrupt();
        cnt = 0;
        contador.setText(Integer.toString(cnt));
        repaint();
      }
    });
    temp.add(btStop);
    JButton btClose = new JButton("Fechar");
    btClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        canvas.setVisible(false);
        System.exit(0);
      }
    });
    temp.add(btClose);
    temp.add(new JLabel("Nº bolas: "));
    temp.add(contador);
    contentPane.add(temp, "South");
    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  class ExpressBall extends Ball implements Runnable {
    public ExpressBall(JPanel b, Color c) {
      super(b, c);
    }

	public void run() {
      try {
        draw();
        while(true) {
          move();
          Thread.sleep(5);
        }
      } catch(InterruptedException e) {
      }
    }
  }
  
  public static void main(String[] args) {
    new BounceExpress();
  }
}
