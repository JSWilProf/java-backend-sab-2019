package Thread.resposta.ball;

import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.*;

import Thread.ball.Ball;

@SuppressWarnings("serial")
class BounceThread extends JFrame {
	private JPanel canvas = new JPanel();
	private int cnt = 0;
	private JLabel contador = new JLabel(Integer.toString(cnt));
	private ExecutorService pool = Executors.newCachedThreadPool(); // <======

	public BounceThread() {
		setTitle("Bounce");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container contentPane = getContentPane();
		canvas.setPreferredSize(new Dimension(450, 200));
		contentPane.add(canvas, "Center");

		JPanel temp = new JPanel();
		JButton btStart = new JButton("Iniciar");
		btStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pool.execute(new ThreadBall(canvas));  // <======
//				ThreadBall b = new ThreadBall(canvas);
//				new Thread(b).start();
				contador.setText(Integer.toString(++cnt));
			}
		});
		temp.add(btStart);
		JButton btClose = new JButton("Fechar");
		btClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				canvas.setVisible(false);
				pool.shutdown(); // <======
				System.exit(0);
			}
		});
		temp.add(btClose);

		temp.add(new JLabel("Nº bolas: "));
		temp.add(contador);
		contentPane.add(temp, "South");
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	class ThreadBall extends Ball implements Runnable {
		public ThreadBall(JPanel b) {
			super(b);
		}

		public void run() {
			try {
				draw();
				while (true) {
					move();
					Thread.sleep(5);
				}
			} catch (InterruptedException e) {
			}
		}
	}

	public static void main(String[] args) {
		new BounceThread();
	}
}
