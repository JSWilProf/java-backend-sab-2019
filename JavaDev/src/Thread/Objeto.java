package Thread;

import java.util.Date;

public class Objeto {
	private String descricao;
	private Date inicio;
	
	public Objeto() {
	  this("sem descrição");	
	}
	
	public Objeto(String descricao) {
		this(descricao, new Date());
	}
	
	public Objeto(String descricao, Date inicio) {
		this.descricao = descricao;
	  this.inicio = inicio;
	}
	
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return "descricao=" + descricao + " Inicio: " + inicio;
	}
	
}
