package Thread;

import java.util.Calendar;
import java.util.Date;

public class UsaObjeto {
	public static void main(String[] args) {
		Objeto obj = new NovoObjeto("Olá");
		Calendar cal = Calendar.getInstance();
		cal.set(2011, Calendar.MARCH, 30); 
		Objeto obj2 = new NovoObjeto("Olá", cal.getTime());
		System.out.println(obj + "\n" + obj2);
	}
}

class NovoObjeto extends Objeto {
	private int codigo;
	
	public NovoObjeto(String texto) {
		super(texto);
		codigo = 123;
	}

	public NovoObjeto(String texto, Date inicio) {
		super(texto, inicio);
		codigo = 123;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return super.toString() + " codigo=" + codigo;
	}
	
}