package Thread;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExemploStream3 {
	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			lista.add( String.valueOf( (int)Math.ceil(Math.random()*1000) ) );
		}
		System.out.println(new Date());
		System.out.println(lista.parallelStream()
				.map(texto -> Integer.parseInt(texto))
				.filter(num -> num >= 400)
				.count() );
		System.out.println(new Date());
	}
}
