package Thread;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExemploStream2 {
	public static void main(String[] args) {
		List<String> lista = new ArrayList<>(10000000);
		for (int i = 0; i < 10000000; i++) {
			lista.add( String.valueOf( (int)Math.ceil(Math.random()*1000) ) );
		}
		System.out.println(new Date());
		System.out.println(lista.parallelStream()
				.map(texto -> Integer.parseInt(texto))
				.filter(num -> num >= 400)
				.count() );
		System.out.println(new Date());
	}
}
