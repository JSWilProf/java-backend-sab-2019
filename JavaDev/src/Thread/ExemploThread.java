package Thread;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ExemploThread extends JFrame  implements ActionListener, Runnable {
	private JButton btIniciar = new JButton("Iniciar");
	private JButton btParar = new JButton("Parar");
	private JTextField tfNum = new JTextField(5);
	private ExemploThread thisInstance = null;
	private Thread processo;

	public ExemploThread() {
		thisInstance = this;
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		p1.add(tfNum);
		add(p1, BorderLayout.CENTER);
	
		btIniciar.addActionListener(this);
		btParar.addActionListener(this);
		btParar.setEnabled(false);
		
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		p2.add(btIniciar);
		p2.add(btParar);
		add(p2, BorderLayout.SOUTH);
		
		setTitle("Tela 1/2 Boca");
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btIniciar)) {
			processo = new Thread(thisInstance);
			processo.start();
			btIniciar.setEnabled(false);
			btParar.setEnabled(true);
		} else {
			processo.interrupt();
			btIniciar.setEnabled(true);
			btParar.setEnabled(false);
		}
	}
	
	@Override
	public void run() {
		int i = 0;
		while (!processo.isInterrupted()) {
			tfNum.setText(String.format("%05d", i));
			i++;
		}
	}
	
	public static void main(String[] args) {
		new ExemploThread();
	}
}







