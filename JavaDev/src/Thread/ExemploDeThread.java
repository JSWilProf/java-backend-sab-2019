package Thread;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ExemploDeThread {
	public static void main(String[] args) {
		Teste t = new Teste();
		Thread th = new Thread(t);
		th.setPriority(Thread.MAX_PRIORITY);
		th.start();
   	    String nome = JOptionPane.
				showInputDialog("informe seu nome");
		JOptionPane.
		       showMessageDialog(null, "Bem vindo, " + nome);
	}

}

@SuppressWarnings("serial")
class Teste extends JFrame  implements Runnable {
	public void run() {
		for(int i = 0;i< 10;) {
			System.out.println("Oi!");
		}
	}
}