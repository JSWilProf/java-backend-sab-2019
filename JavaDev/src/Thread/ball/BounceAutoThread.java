package Thread.ball;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


// Demonstração do uso do método sleep() em Thread
// e as vantagens deste em relação a um loop com "while"

@SuppressWarnings("serial")
class BounceAutoThread extends JFrame {
  private JPanel canvas = new JPanel();
  private int cnt = 0;
  private JLabel contador = new JLabel(Integer.toString(cnt));
  private ThreadGroup tg = new ThreadGroup("bolas");

  public BounceAutoThread() {
    setTitle("Bounce");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Container contentPane = getContentPane();
    canvas.setPreferredSize(new Dimension(450, 200));
    contentPane.add(canvas, "Center");

    JPanel temp = new JPanel();
    JButton btStart = new JButton("Iniciar");
    btStart.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Thread t = new Thread(tg, new ColorBall(canvas, Color.black));
        t.setPriority(Thread.NORM_PRIORITY);
        t.start();
        contador.setText(Integer.toString(++cnt));
      }
    });
    temp.add(btStart);
    JButton btAuto = new JButton("AutoThread");
    btAuto.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Thread t = new Thread(tg, new SelfishBall(canvas, Color.blue));
        t.setPriority(Thread.MAX_PRIORITY);
        t.start();
        contador.setText(Integer.toString(++cnt));
      }
    });
    temp.add(btAuto);
    JButton btStop = new JButton("Parar");
    btStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        tg.interrupt();
        cnt = 0;
        contador.setText(Integer.toString(cnt));
        repaint();
      }
    });
    temp.add(btStop);
    JButton btClose = new JButton("Fechar");
    btClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        canvas.setVisible(false);
        System.exit(0);
      }
    });
    temp.add(btClose);

    temp.add(new JLabel("Nº bolas: "));
    temp.add(contador);

    contentPane.add(temp, "South");
    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  class ColorBall extends Ball implements Runnable {
    public ColorBall(JPanel b, Color c) {
      super(b, c);
    }

    public void run() {
      try {
        draw();
        while(true) {
          move();
          Thread.sleep(5);
        }
      } catch(InterruptedException e) {
      }
    }
  }

  class SelfishBall extends Ball implements Runnable {
    public SelfishBall(JPanel b, Color c) {
      super(b, c);
    }

    public void run() {
      draw();
      while(true) {
        move();
        long t = System.currentTimeMillis();
        while(System.currentTimeMillis() < t + 5) {}
      }
    }
  }

  public static void main(String[] args) {
    new BounceAutoThread();
  }
}
