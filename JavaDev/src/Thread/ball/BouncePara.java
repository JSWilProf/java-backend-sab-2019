package Thread.ball;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
class BouncePara extends JFrame {
	private JPanel canvas = new JPanel();
	private ThreadGroup grupo = new ThreadGroup("grp");
	private boolean parar = true;
	private JLabel contador = new JLabel("Nº Bolas");
	private List<SimpleBall> bolas = 
		new ArrayList<SimpleBall>();

	public BouncePara() {
		setTitle("Bounce");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container contentPane = getContentPane();
		canvas.setPreferredSize(new Dimension(450, 200));
		contentPane.add(canvas, "Center");

		JPanel temp = new JPanel();
		JButton btStart = new JButton("Iniciar");
		btStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleBall b = new SimpleBall(canvas);
				bolas.add(b);
				Thread t = new Thread(grupo, b);
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
				SimpleBall b2 = new SimpleBall(canvas, Color.red);
				bolas.add(b2);
				Thread t2 = new Thread(grupo, b2);
				t2.setPriority(Thread.MAX_PRIORITY);
				t2.start();
				contador.setText("Nº Bolas " + bolas.size());
			}
		});
		temp.add(btStart);
		JButton btLimpar = new JButton("Limpar");
		btLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				grupo.interrupt();
				while(grupo.activeCount() > 0) {}
				bolas.clear();
				contador.setText("Nº Bolas " + bolas.size());
				repaint();
			}
		});
		temp.add(btLimpar);
		final JButton btParar = new JButton("Para");
		btParar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(parar) {
				  btParar.setText("Continua");
				  parar = false;
				} else {
				  btParar.setText("Para");
				  parar = true;
				}
			  for (SimpleBall b : bolas)
					b.para();
			}
		});
		temp.add(btParar);
		JButton btClose = new JButton("Fechar");
		btClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				canvas.setVisible(false);
				System.exit(0);
			}
		});
		temp.add(btClose);
		temp.add(contador);
		contentPane.add(temp, "South");
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	class SimpleBall extends Ball implements Runnable {
		private boolean mostrar = true;
		
		public SimpleBall(JPanel b) {
			super(b);
		}

		public SimpleBall(JPanel b, Color c) {
			super(b,c);
		}

		public void para() {
			if(mostrar)
				mostrar = false;
			else
				mostrar = true;
		}
		
		public void run() {
			try {
				draw();
				//for (;;) {
				while(true){
					if(mostrar)
					   move();
					Thread.sleep(5);
				}
			} catch (InterruptedException e) {
			}
		}
	}

	public static void main(String[] args) {
		new BouncePara();
	}
}
