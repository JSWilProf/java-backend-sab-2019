package Thread;

public class Impressao {
	public void cabecalho(String texto) {
	}

	public void detalhe(String linha) {
	}

	public void rodape(String texto) {
	}
}

// --------------------------------

class Relatorio extends Impressao implements Runnable {
	private void processaRelatorio() {
		try {
			cabecalho("Relatorio Gerencial");
			for (int i = 0; i < 20; i++) {
				detalhe("linha " + i);
				Thread.sleep(5000);
			}
			rodape("feito por Nois! :D");
		} catch (InterruptedException e) {
			System.out.println("Relatorio Cancelado pelo usuario");
		}
	}

	@Override
	public void run() {
		processaRelatorio();
	}
}