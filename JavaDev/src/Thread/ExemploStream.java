package Thread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExemploStream {
	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();
		for (int i = 0; i < 2000; i++) {
			lista.add( String.valueOf( (int)Math.ceil(Math.random()*1000) ) );
		}
		
		String msg = "";
		Collections.sort(lista);
		for (String texto : lista) {
			int num = Integer.parseInt(texto);
			if(num >= 400) {
				msg += texto + "\n";
			}
		}
		System.out.println(msg);
	}
}
