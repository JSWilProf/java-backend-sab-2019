package Thread.waitNotify;

import java.util.LinkedList;

public class Fila {
	public static void main(String[] argv) {
		FilaDeTrabalho fila = new FilaDeTrabalho();

		int qtdOperarios = 5;
		Operario[] lista = new Operario[qtdOperarios];
		for (int i = 0; i < lista.length; i++) {
			lista[i] = new Operario(i, fila);
			lista[i].start();
		}

		for (int i = 0; i < 10000; i++)
			fila.passaTrampo(i);
		
		for (int i = 0; i < lista.length; i++)
			lista[i].interrupt();
	}
}

class FilaDeTrabalho {
	LinkedList<Object> queue = new LinkedList<Object>();

	public synchronized void passaTrampo(Object o) {
		queue.addLast(o);
		notify();
	}

	public synchronized Object pegaTrampo() throws InterruptedException {
		while (queue.isEmpty()) {
			wait();
		}
		return queue.removeFirst();
	}
}

class Operario extends Thread {
	FilaDeTrabalho q;
	int id;

	Operario(int id, FilaDeTrabalho q) {
		this.q = q;
		this.id = id;
	}

	public void run() {
		try {
			while (true) {
				Object x = q.pegaTrampo();

				if (x == null)
					break;

				System.out.println(id + ":  " + x);
			}
		} catch (InterruptedException e) {
		}
	}
}