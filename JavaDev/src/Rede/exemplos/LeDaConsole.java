package Rede.exemplos;

import javax.swing.JOptionPane;

public class LeDaConsole {
	public static void main(String[] args) {
		// estabelecimento da conexão
		while (true) {
			String msg = JOptionPane.showInputDialog("Informe a mensagem");
			System.out.println(msg);
			// conversação
			if (msg.equals("SAIR"))
				break;
		}
		// desconexão
	}
}
