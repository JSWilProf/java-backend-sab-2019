package Rede.exemplos.cliente;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import javax.swing.*;

@SuppressWarnings("serial")
public class ClienteNovo extends JFrame implements ActionListener {
  private JTextArea taInput = new JTextArea(10, 15);
  private JTextField tfMsg = new JTextField(20);
  private BufferedReader in = null;
  private BufferedWriter out = null;
  private Socket skt = null;
  private Thread t;

  public ClienteNovo() {
    setTitle("Chat 1/2 Boca");
    Container fundo = getContentPane();

    JMenuBar bar = new JMenuBar();
    JMenu menu = new JMenu("Ação");
    JMenuItem item = new JMenuItem("Conectar");
    item.addActionListener(this);
    item.setMnemonic(KeyEvent.VK_C);
    menu.add(item);
    item = new JMenuItem("Desconectar");
    item.addActionListener(this);
    item.setMnemonic(KeyEvent.VK_D);
    menu.add(item);
    bar.add(menu);
    setJMenuBar(bar);

    JPanel campos = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
    campos.add(new JLabel("Mens."));
    campos.add(tfMsg);
    tfMsg.setEnabled(false);
    tfMsg.addActionListener(this);
    tfMsg.setActionCommand("Enviar");
    fundo.add(campos, "North");

    taInput.setEditable(false);
    JScrollPane sp = new JScrollPane(taInput);
    sp.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      public void adjustmentValueChanged(AdjustmentEvent e) {
        taInput.setCaretPosition(taInput.getText().length());
      }
    });
    fundo.add(sp, "Center");

    pack();
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent ev) {
    String cmd = ev.getActionCommand();
    
    try {
      if(cmd.equals("Enviar")) {
        out.write(tfMsg.getText() + "\n");
        out.flush();
        tfMsg.setText("");
      } else if(cmd.equals("Conectar")) {
        conecta();
        tfMsg.setEnabled(true);
        Runnable r = new Runnable() {
          public void run() {
            try {
              while(!t.isInterrupted()) {
            	if(in.ready())  
            		taInput.append(in.readLine() + "\n");
              }
            }
            catch(IOException ex) {
              System.out.println(ex.getMessage());
            }
          }
        };
        t = new Thread(r);
        t.start();
      } else if(cmd.equals("Desconectar")) {
        out.write("SAIR\n");
        out.flush();
        
        t.interrupt();
        tfMsg.setEnabled(false);
        if(skt != null) skt.close();
      }
    }
    catch(IOException ex) {
      JOptionPane.showMessageDialog(this, "Erro durante a conexão: "
        + ex.getMessage());
    }
  }

  private void conecta() throws IOException {
    skt = new Socket("localhost", 1234);
    InputStreamReader is = new InputStreamReader(skt.getInputStream());
    in = new BufferedReader(is);
    OutputStreamWriter os = new OutputStreamWriter(skt.getOutputStream());
    out = new BufferedWriter(os);
    taInput.append(skt.toString() + "\n");
  }

  public static void main(String[] args) {
    new ClienteNovo();
  }
}
