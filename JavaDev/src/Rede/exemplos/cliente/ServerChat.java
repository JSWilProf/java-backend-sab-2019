package Rede.exemplos.cliente;

import java.io.*;
import java.net.*;

public class ServerChat extends Thread {
	private Socket skt;
	
	public ServerChat(Socket skt) {
		this.skt = skt;
	}

	private void envia(PrintWriter out, String msg) {
		out.println(msg);
		out.flush();
	}
	
	private String recebe(BufferedReader in) throws IOException {
		String linha = null;
		while(!in.ready()) {
			linha = in.readLine();
		}
		return linha;
	}
	
	public void run() {
		try {
			InputStreamReader is = new InputStreamReader(skt.getInputStream());
			BufferedReader in = new BufferedReader(is);
			PrintWriter out = new PrintWriter(skt.getOutputStream());

			envia(out, "Bem Vindo");

			String txt = recebe(in);
			while (!txt.equals("SAIR")) {
				envia(out, txt);

				txt = recebe(in);
			}
			skt.close();

		} catch (IOException ex) {
			System.out.println("Erro durante a conexão: " + ex.getMessage());
		}

	}

	public static void main(String[] args) {
		try ( ServerSocket srv = new ServerSocket(1234); ) {
			while (true) {
				Socket skt = srv.accept();
				ServerChat server = new ServerChat(skt);
				server.start();
			}
		} catch (IOException ex) {
			System.out.println("Erro durante a conexão: " + ex.getMessage());
		}
	}
}
