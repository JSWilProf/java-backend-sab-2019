package Rede.exemplos.cliente;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Tela1 extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblMensagem;
	private JTextField tfMsg;
	private JButton btnEnviar;
	private JScrollPane scrollPane;
	private JTextArea txt;
	private JButton btnConectar;
	private JButton btnDesconectar;
	private JButton btnSair;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tela1 frame = new Tela1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tela1() {
		setTitle("Chat 1/2 Boca!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblMensagem = new JLabel("Mensagem");
		lblMensagem.setBounds(6, 16, 75, 16);
		contentPane.add(lblMensagem);
		
		tfMsg = new JTextField();
		tfMsg.setBounds(93, 15, 223, 19);
		contentPane.add(tfMsg);
		tfMsg.setColumns(10);
		
		btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(this);
		btnEnviar.setBounds(338, 12, 106, 25);
		contentPane.add(btnEnviar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 44, 438, 183);
		contentPane.add(scrollPane);
		
		txt = new JTextArea();
		scrollPane.setViewportView(txt);
		
		btnConectar = new JButton("Conectar");
		btnConectar.addActionListener(this);
		btnConectar.setBounds(28, 247, 106, 25);
		contentPane.add(btnConectar);
		
		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.addActionListener(this);
		btnDesconectar.setBounds(162, 247, 124, 25);
		contentPane.add(btnDesconectar);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		btnSair.setBounds(314, 247, 106, 25);
		contentPane.add(btnSair);
	}

	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btnConectar)) {
			
		} else if(botao.equals(btnDesconectar)) {
			
		} else if (botao.equals(btnEnviar)) {
			
		} else {
			System.exit(0);
		}
	}
}
