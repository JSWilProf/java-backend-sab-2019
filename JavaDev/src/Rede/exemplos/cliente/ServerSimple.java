package Rede.exemplos.cliente;

import java.io.*;
import java.net.*;

public class ServerSimple {
	public static void main(String[] args) {
		ServerSocket server = null;
		try {
			// Configurando a porta do Servidor
			server = new ServerSocket(1234);

			while (true) {
				// Aguardando a conexão de um cliente na porta configurada
				Socket s = server.accept();

				// Mostar o início da conexão na console do servidor
				System.out.println("Iniciando Conexão com: "
						+ s.getInetAddress());

				// Obtém o canal de leitura do cliente
				InputStreamReader is = new InputStreamReader(s.getInputStream());
				BufferedReader in = new BufferedReader(is);

				// Obtém o canal de gravação ao cliente
				OutputStreamWriter os = new OutputStreamWriter(
						s.getOutputStream());
				PrintWriter out = new PrintWriter(os);

				// Envia mensagem de boas vindas para o cliente
				out.println("Bem vindo " + s.getInetAddress());
				out.flush();

				// Rotina para a conversa com o cliente
				while (true) {
					// Lê o cliente
					String txt = in.readLine();

					// Grava no cliente
					out.write("S:" + txt + "\n");
					out.flush();

					// Se a string lida for SAIR finaliza a conversa
					if (txt == null || txt.equals("SAIR"))
						break;
				}
				// Mostar a finalização da conexão na console do servidor
				System.out.println("Finalizando Conexão com: "
						+ s.getInetAddress());

				// Fecha a conexão com o cliente
				s.close();
			}
		} catch (IOException ex) {
			System.out.println("Erro durante a conexão: " + ex.getMessage());
		} finally {
			try {
				if (server != null)
					server.close();
			} catch (Exception e) {
			}
		}
	}
}
