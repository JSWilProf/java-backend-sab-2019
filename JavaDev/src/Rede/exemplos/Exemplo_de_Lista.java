package Rede.exemplos;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Exemplo_de_Lista {
	private static List<PrintWriter> lista = new ArrayList<>();
	
	
	public static void main(String[] args) {
		String mensagem = "Olá";
		
		PrintWriter out = new PrintWriter(System.out);
		lista.add(out);
		
		
		for (int i = 0; i < lista.size(); i++) {
			PrintWriter sout = lista.get(i);
			sout.println(mensagem);
			sout.flush();
		}
		
		
		for (PrintWriter sout : lista) {
			sout.println(mensagem);
			sout.flush();
		}
		
		
		lista.remove(out);
	}
}
