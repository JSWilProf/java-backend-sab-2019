package Rede.exemplos.servers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServerThreadId extends Thread {
	private Socket s = null;
	private int id = 0;

	public EchoServerThreadId(Socket skt, int id) {
		s = skt;
		this.id = id;
	}

	public void run() {
		try {
			InputStreamReader is = new InputStreamReader(s.getInputStream());
			BufferedReader in = new BufferedReader(is);
			PrintWriter out = new PrintWriter(s.getOutputStream());
			out.println("Bem Vindo " + id);
			out.flush();

			String msg = "";
			while (msg != null && !msg.equals("SAIR")) {
				msg = in.readLine();

				out.println(id + ":" + msg);
				out.flush();
			}

			s.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		System.setProperty("java.net.preferIPv4Stack", "true");
		try (ServerSocket server = new ServerSocket(1234)) {
			for (int i = 1;; i++) {
				Socket s = server.accept();
				EchoServerThreadId c = new EchoServerThreadId(s, i);
				c.start();
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} 
	}
}