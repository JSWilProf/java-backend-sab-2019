package Rede.exemplos.servers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class CharServerThreadId extends Thread {
	private Socket s = null;
	private int id = 0;
	private static List<PrintWriter> lista = new ArrayList<PrintWriter>();

	public CharServerThreadId(Socket skt, int id) {
		s = skt;
		this.id = id;
	}

	public void run() {
		try {
			InputStreamReader is = new InputStreamReader(s.getInputStream());
			BufferedReader in = new BufferedReader(is);
			PrintWriter out = new PrintWriter(s.getOutputStream());

			// incluir o out na lista
			synchronized (lista) {
				lista.add(out);
			}

			out.println("Bem Vindo " + id);
			out.flush();

			String msg = "";
			while (msg != null && !msg.equals("SAIR")) {
				msg = in.readLine();

				// acessar o out para gravar
				synchronized (lista) {
					for (PrintWriter sout : lista) {
						sout.println(id + ":" + msg);
						sout.flush();
					}
				}

			}
			
			// retirar o out da lista
			synchronized (lista) {
				lista.remove(out);
			}
			s.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		 try (ServerSocket server = new ServerSocket(1234)) {
			for (int i = 1;; i++) {
				Socket s = server.accept();
				CharServerThreadId c = new CharServerThreadId(s, i);
				c.start();
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} 
	}
}