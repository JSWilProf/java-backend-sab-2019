package Rede.exemplos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Servidor3 {
	public static void main(String[] args) {
		try (ServerSocket server = new ServerSocket(1234);) {
			int id = 0;

			while (true) {
				// Inicia a Conexão
				Socket skt = server.accept();
				System.out.println("conectou " + skt.getInetAddress());
				new Chat3(skt, ++id).start();
			}

		} catch (IOException ex) {
			System.out.println("ERRO:\n" + ex.getMessage());
		}
	}
}

class Chat3 extends Thread {
	private static List<PrintWriter> lista = new ArrayList<>();
	private Socket skt;
	private int id;

	public Chat3(Socket skt, int id) {
		this.skt = skt;
		this.id = id;
	}

	public void run() {
		try {
			// Configura os canais de entrada e saída
			BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			PrintWriter out = new PrintWriter(skt.getOutputStream());

			synchronized (lista) {
				lista.add(out);
				System.out.println(lista.size());
			}
			
			out.println("Bem Vindo " + id);
			out.flush();

			String msg = "";
			do {
				if (in.ready()) {
					msg = in.readLine();

					synchronized (lista) {
						for (PrintWriter writer : lista) {
							writer.println(id + ": " + msg);
							writer.flush();
						}
						System.out.println(msg);
					}
				}
			} while (!msg.equals("SAIR"));

			synchronized (lista) {
				lista.remove(out);
				System.out.println(lista.size());
			}
			
			skt.close();
			System.out.println("Fim");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}