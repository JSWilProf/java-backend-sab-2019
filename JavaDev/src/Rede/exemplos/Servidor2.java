package Rede.exemplos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor2 {
	public static void main(String[] args) {
		try (ServerSocket server = new ServerSocket(1234);) {
			int id = 0;

			while (true) {
				// Inicia a Conexão
				Socket skt = server.accept();
				new Chat(skt, ++id).start();
			}

		} catch (IOException ex) {
			System.out.println("ERRO:\n" + ex.getMessage());
		}
	}
}

class Chat extends Thread {
	private Socket skt;
	private int id;

	public Chat(Socket skt, int id) {
		this.skt = skt;
		this.id = id;
	}

	public void run() {
		try {
			// Configura os canais de entrada e saída
			BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			PrintWriter out = new PrintWriter(skt.getOutputStream());

			out.println("Bem Vindo " + id);
			out.flush();

			String msg = "";
			do {
				if (in.ready()) {
					msg = in.readLine();

					out.println(id + ": " + msg);
					out.flush();
				}
			} while (!msg.equals("SAIR"));

			skt.close();
			System.out.println("Fim");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}