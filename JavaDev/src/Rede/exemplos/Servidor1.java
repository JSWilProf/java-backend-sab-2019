package Rede.exemplos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor1 {
	public static void main(String[] args) {
		try (ServerSocket server = new ServerSocket(1234);) {
			// Inicia a Conexão
			Socket skt = server.accept();
			int i = 0;
			
			// Configura os canais de entrada e saída
			BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			PrintWriter out = new PrintWriter(skt.getOutputStream());

			out.println("Bem Vindo " + ++i);
			out.flush();

			
			String msg = "";
			do {
				if (in.ready()) {
					msg = in.readLine();

					out.println(i + ": " + msg);
					out.flush();
				}
			} while (!msg.equals("SAIR"));
			
			skt.close();
			System.out.println("Fim");
		} catch (IOException ex) {
			System.out.println("ERRO:\n" + ex.getMessage());
		}
	}
}
