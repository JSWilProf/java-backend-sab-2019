package Rede.old.Rmi.bounce;

import java.awt.Color;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class MeuServidor extends UnicastRemoteObject implements BounceInterface {
	private static final long serialVersionUID = 1L;

	protected MeuServidor() throws RemoteException {
		super();
	}

	@Override
	public void createBall(Color c) throws RemoteException {
		System.out.println(c);
	}
	
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		 System.setProperty("java.net.preferIPv4Stack", "true");
		 Registry reg = LocateRegistry.createRegistry(1099);
		 reg.bind("ball", new MeuServidor());
	}
}
