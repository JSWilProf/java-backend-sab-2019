package Rede.old.Rmi.bounce;

import java.awt.Color;
import java.rmi.Naming;

import javax.swing.JOptionPane;

public class MinhaBola {
	public static void main(String[] args) {
		try {
			System.setSecurityManager(new SecurityManager());

			BounceInterface bi = (BounceInterface)Naming.lookup("rmi://10.84.144.250/ball");
			
			bi.createBall(new Color((int) Math.ceil(Math.random() * 255), 
									(int) Math.ceil(Math.random() * 255), 
									(int) Math.ceil(Math.random() * 255)));
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
			ex.printStackTrace();
		}
	}
}
