package Rede.old.Rmi.old;

import javax.naming.*;

public class ListJNDI {
  public static void main(String[] args) {
    new ListJNDI("10.190.39.250");
  }

  @SuppressWarnings({ "rawtypes" })
public ListJNDI(String host) {
    try {
    	// VIde: https://docs.jboss.org/author/display/AS71/EJB+invocations+from+a+remote+client+using+JNDI
      //JBoss
 //     System.setProperty("java.naming.factory.initial", 
    		  //"org.jboss.naming.remote.client.InitialContextFactory");
 //   		  "org.jnp.interfaces.NamingContextFactory");
 //     System.setProperty("java.naming.factory.url.pkgs", 
    		  //"org.jboss.ejb.client.naming");
 //   		  "org.jboss.naming:org.jnp.interfaces");
 //     System.setProperty("java.naming.provider.url",  "jnp://"+host+":1099");
//        String local = "";

      //WebSphere 
//      System.setProperty("java.naming.factory.initial",   "com.sun.jndi.cosnaming.CNCtxFactory");
//      System.setProperty("java.naming.provider.url", "iiop://" + host + ":2809");
//      String local = "cell/nodes/" + host + "Node01/servers/server1";

      //GlassFish
//      System.setProperty("java.naming.factory.initial",   "com.sun.jndi.cosnaming.CNCtxFactory");
//      System.setProperty("java.naming.provider.url", "iiop://" + host + ":8686");
      String local = "";

      Context ctx = new InitialContext();
      NamingEnumeration items = ctx.list(local);
      list(ctx, items, local);
      
      //RMI
      //list(java.rmi.Naming.list(local), items, local);
      System.exit(0);
    } catch(NamingException e) {
      System.out.println(e.getMessage());
    }
  }

  @SuppressWarnings({"rawtypes" })
  public void list(Context ctx, NamingEnumeration items, String local)
      throws NamingException {
    while(items.hasMoreElements()) {
      NameClassPair item = (NameClassPair)items.next();

      try {
        if(!("thisNode".equals(item.getName()) || "cell".equals(item.getName()))) {
          String localName = local + "/" + item.getName();
          System.out.println(localName);
          NamingEnumeration newitems = ctx.list(localName);
          list(ctx, newitems, localName);
        }
      } catch(NotContextException ex) {
      }
    }
  }

}
