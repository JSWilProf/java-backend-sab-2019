package Rede.old.outros;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor2 {
	public static void main(String[] args) throws Exception {
		try (ServerSocket srv = new ServerSocket(1234);) {
			while (true) {
				Socket skt = srv.accept();
				Servidor2 s = new Servidor2();
				Thread t = s.new Chat(skt);
				t.start();
			}
		}
	}

	class Chat extends Thread {
		private Socket skt = null;

		public Chat(Socket skt) {
			this.skt = skt;
		}

		public void run() {
			try {
				// ----------------------------------------------
				BufferedReader in = new BufferedReader(new InputStreamReader(
						skt.getInputStream()));
				PrintWriter out = new PrintWriter(skt.getOutputStream());

				out.println("Bem Vindo!");
				out.flush();

				String msg = "";
				while (!msg.equals("SAIR")) {
					msg = in.readLine();
					out.println("S: " + msg);
					out.flush();
				}

				skt.close();
				// ----------------------------------------------
			} catch (Exception ex) {
			}
		}
	}
}
