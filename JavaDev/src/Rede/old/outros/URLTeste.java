package Rede.old.outros;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

@SuppressWarnings("serial")
public class URLTeste extends JFrame {
  JTextField tfUrl = new JTextField(25);
  JTextArea taPagina = new JTextArea(20, 40);

  public URLTeste() {
    Container painel = getContentPane();
    painel.setLayout(new BorderLayout());
    painel.add(new JScrollPane(taPagina), BorderLayout.NORTH);
    
    JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
    p1.add(new JLabel("URL"));
    p1.add(tfUrl);
    p1.add(Box.createRigidArea(new Dimension(15, 10)));
    JButton btConnect = new JButton("Conectar");
    btConnect.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        try {
          URL url = new URL(tfUrl.getText());
          BufferedReader in = new BufferedReader(
        		  new InputStreamReader(url.openStream()));
          String linha = null;

          while((linha = in.readLine()) != null)
            taPagina.append(linha + "\n");
        } catch(MalformedURLException ex) {
          taPagina.setText("Erro: " + ex.getMessage());
        } catch(IOException ex) {
          taPagina.setText("Erro: " + ex.getMessage());
        }
      }
    });
    p1.add(btConnect);
    p1.add(Box.createRigidArea(new Dimension(15, 10)));
    JButton btFechar = new JButton("Fechar");
    btFechar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        setVisible(false);
        System.exit(0);
      }
    });
    p1.add(btFechar);
    painel.add(p1, BorderLayout.SOUTH);
    
    setTitle("URL Teste");
    pack();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setVisible(true);
    tfUrl.requestFocus();
    getRootPane().setDefaultButton(btConnect);
  }

  public static void main(String[] args) {
    new URLTeste();
  }
}
