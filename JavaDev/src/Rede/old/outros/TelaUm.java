package Rede.old.outros;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class TelaUm extends JFrame {
	private JPanel contentPane;
	private JTextField tfNome;
	private JTextField tfEnd;
	private JTextField tfFone;
	String valor;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaUm frame = new TelaUm();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaUm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 369, 206);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("inicio");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmConectar = new JMenuItem("conectar");
		mntmConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 valor = "sim";
			}
		});
		mnNewMenu.add(mntmConectar);
		
		JMenuItem mntmSair = new JMenuItem("sair");
		mnNewMenu.add(mntmSair);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel campos = new JPanel();
		contentPane.add(campos, BorderLayout.CENTER);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 14, 40, 15);
		
		tfNome = new JTextField();
		tfNome.setBounds(64, 12, 211, 19);
		tfNome.setColumns(10);
		
		JLabel lblEnd = new JLabel("End.");
		lblEnd.setBounds(12, 51, 31, 15);
		
		tfEnd = new JTextField();
		tfEnd.setBounds(64, 49, 282, 19);
		tfEnd.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(12, 88, 62, 15);
		
		tfFone = new JTextField();
		tfFone.setBounds(86, 86, 114, 19);
		tfFone.setColumns(10);
		campos.setLayout(null);
		campos.add(lblNome);
		campos.add(tfNome);
		campos.add(lblEnd);
		campos.add(tfEnd);
		campos.add(lblTelefone);
		campos.add(tfFone);
		
		JPanel botoes = new JPanel();
		contentPane.add(botoes, BorderLayout.SOUTH);
		botoes.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 5));
		
		JButton btOk = new JButton("Ok");
		btOk.setMnemonic('O');
		btOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = tfNome.getText();
				String end = tfEnd.getText();
				String fone = tfFone.getText();
				
				JOptionPane.showMessageDialog(null, 
					"Nome: " + nome +
					"\nEnd.: " + end +
					"\nFone: " + fone + "\n" + valor);
				
				tfNome.setText("");
				tfNome.requestFocus();
				tfEnd.setText("");
				tfFone.setText("");
			}
		});
		botoes.add(btOk);
		
		JButton btSair = new JButton("Sair");
		btSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		botoes.add(btSair);
		
	}
}
