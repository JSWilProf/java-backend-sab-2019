package Rede.old.outros;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class TelaChat extends JFrame {
	private JPanel contentPane;
	private JTextField tfMsg;

	private Socket skt;
	private BufferedReader in;
	private PrintWriter out;
	private Thread leitor;
	private JTextArea textArea = new JTextArea();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaChat frame = new TelaChat();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaChat() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 319);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnAes = new JMenu("Ações");
		menuBar.add(mnAes);

		JMenuItem mntmConectar = new JMenuItem("Conectar");
		mntmConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// iniciar
				try {
					skt = new Socket("10.190.39.250", 1234);
					InputStreamReader is = new InputStreamReader(skt
							.getInputStream());
					in = new BufferedReader(is);
					out = new PrintWriter(skt.getOutputStream());

					leitor = new Thread() {
						public void run() {
							try {
								while (!interrupted()) {
									String txt = in.readLine();
									textArea.append(txt + "\n");
									Thread.sleep(5);
								}
							} catch (Exception ex) {
							}
						}
					};
					leitor.start();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,
							"Falha ao conectar no servidor");
				}
			}
		});
		mnAes.add(mntmConectar);

		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// finalizar
				if (skt != null) {
					try {
						leitor.interrupt();
						skt.close();
					} catch (Exception ex) {
					}
				}
				System.exit(0);
			}
		});
		mnAes.add(mntmSair);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMensagem = new JLabel("Mensagem");
		lblMensagem.setBounds(12, 12, 97, 15);
		contentPane.add(lblMensagem);

		tfMsg = new JTextField();
		tfMsg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				// enviar
				try {
					String txt = tfMsg.getText();
					out.println(txt);
					out.flush();
					tfMsg.setText(""); // limpar o campo
					tfMsg.requestFocus(); // solicitanto o cursor
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Falha no envio da mensagem");
				}
			}
		});
		tfMsg.setBounds(122, 10, 310, 19);
		contentPane.add(tfMsg);
		tfMsg.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 39, 420, 222);
		contentPane.add(scrollPane);

		scrollPane.setViewportView(textArea);
	}
}
