package Rede.old.outros;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TesteConsole {
	public static void main(String[] args) {
		try {
			InputStreamReader ins = new InputStreamReader(System.in);
			BufferedReader entrada = new BufferedReader(ins);

			while (true) {
				System.out.print("Informe seu nome: ");
				String texto = entrada.readLine();
				if(texto.equals("SAIR"))
						break;
				
				System.out.println("Bem Vindo: " + texto);
			}
			System.out.println("Fim");
		} catch (IOException e) {
			System.out.println("Xiiii Marquiho :(");
		}
	}
}
