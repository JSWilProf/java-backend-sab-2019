package Rede.old.outros;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Addr {
	public static void main(String[] args) {
		try {
			InetAddress[] addr = 
				InetAddress.getAllByName("www.google.com.br");
			for (InetAddress ip : addr) {
				System.out.println(
						"Host: " + ip.getCanonicalHostName() + 
						" IP: " + ip.getHostAddress());
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
