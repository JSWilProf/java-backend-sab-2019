package Rede.old.outros.chat;

import java.io.*;
import java.net.*;

public class EchoClient {
	public static void main(String[] args) {
		Socket s = null;
		try {
			s = new Socket("localhost", 1234);
			s.setSoTimeout(10000);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					s.getInputStream()));
			OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
			BufferedReader cin = new BufferedReader(new InputStreamReader(
					System.in));

			System.out.println(s.toString());

			while (true) {
				do {
					System.out.println(in.readLine());
				} while (in.ready());

				System.out.print("Console: ");
				String cInput = cin.readLine();

				out.write(cInput + "\r\n");
				out.flush();

				if (cInput.equals("SAIR"))
					break;
			}
		} catch (IOException ex) {
			System.out.println("Erro durante a conexão: " + ex.getMessage());
		} finally {
			try {
				if (s != null)
					s.close();
			} catch (Exception e) {
			}
		}
	}
}
