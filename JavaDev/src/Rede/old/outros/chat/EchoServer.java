package Rede.old.outros.chat;

import java.io.*;
import java.net.*;

public class EchoServer extends Thread {
	private Socket cliente = null;
	private int count = 0;

	public EchoServer(Socket cli, int i) {
		count = i;
		cliente = cli;
	}

	public void run() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					cliente.getInputStream()));
			PrintWriter out = new PrintWriter(cliente.getOutputStream(), true);

			out.println("Alo! Digite SAIR para finalizar.");
			out.flush();

			while (true) {
				String line = in.readLine();

				out.println("Echo(" + count + "): " + line + "\n");
				out.flush();

				if (line.trim().equals("SAIR"))
					break;
			}
			cliente.close();
		} catch (IOException ex) {
			System.out.println(ex.getLocalizedMessage());
		}
	}

	public static void main(String[] args) {
		int i = 1;
		ServerSocket s = null;
		try {
			s = new ServerSocket(1234);
			while (true) {
				new EchoServer(s.accept(), i++).start();
			}
		} catch (Exception e) {
		} finally {
			try {
				if (s != null)
					s.close();
			} catch (Exception e) {
			}
		}
	}
}
