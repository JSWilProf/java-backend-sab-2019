package Rede.old.outros.chat;

import java.io.*;
import java.net.*;
import java.util.*;

public class EchoChat extends Thread {
  private Socket cliente = null;
  private int count = 0;
  private static List<PrintWriter> lista = new ArrayList<PrintWriter>();

  public EchoChat(Socket cli, int i) {
    count = i;
    cliente = cli;
  }

  public void run() {
    try {
      BufferedReader in = new BufferedReader(new InputStreamReader(cliente
        .getInputStream()));
      PrintWriter myOut = new PrintWriter(cliente.getOutputStream(), true);

      synchronized(lista) {
        lista.add(myOut);
      }

      myOut.println("Alo! Digite SAIR para finalizar.");

      boolean done = false;

      while(!done) {
        String line = in.readLine();

        if(line == null)
          done = true;
        else {
          synchronized(lista) {
            for(PrintWriter out : lista) {
              synchronized(out) {
                out.println("Echo(" + count + "): " + line + "\n");
                out.flush();
              }
            }
          }

          if(line.trim().equals("SAIR"))
            done = true;
        }
      }
      cliente.close();
    }
    catch(IOException ex) {}
  }

  public static void main(String[] args) {
    int i = 1;
    ServerSocket s =  null;
    try {
      s = new ServerSocket(1234);

      while(true) {
        new EchoChat(s.accept(), i++).start();
      }
    }
    catch(Exception e) {}
    finally {
    	try {
    	  if(s != null)
    		  s.close();
    	} catch(Exception e) {}
    }
  }
}
