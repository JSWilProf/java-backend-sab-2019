package Rede.old.outros;

import java.io.*;
import java.net.URL;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.html.*;

@SuppressWarnings("serial")
public class HTMLTeste extends JFrame implements HyperlinkListener,
    ActionListener {
  JEditorPane epPagina = new JEditorPane();
  JTextField tfUrl = new JTextField(25);
  JButton btConnect = new JButton("Conectar");
  JButton btFechar = new JButton("Fechar");

  public HTMLTeste() {
    Container painel = getContentPane();
    painel.setLayout(new BoxLayout(painel, BoxLayout.Y_AXIS));
    epPagina.setEditable(false);
    epPagina.setPreferredSize(new Dimension(640, 480));
    epPagina.addHyperlinkListener(this);
    painel.add(new JScrollPane(epPagina));

    JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
    p1.add(new JLabel("URL"));
    p1.add(tfUrl);
    p1.add(Box.createRigidArea(new Dimension(15, 10)));
    btConnect.addActionListener(this);
    p1.add(btConnect);
    p1.add(Box.createRigidArea(new Dimension(15, 10)));
    btFechar.addActionListener(this);
    p1.add(btFechar);
    p1.setMaximumSize(p1.getPreferredSize());
    painel.add(p1);

    setTitle("HTML Teste");
    pack();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setVisible(true);
    tfUrl.requestFocus();
    getRootPane().setDefaultButton(btConnect);
  }

  public void actionPerformed(ActionEvent ev) {
    if(ev.getSource() == btConnect) {
      try {
        epPagina.setPage(new URL(tfUrl.getText()));
      } catch(IOException ex) {
        JOptionPane.showMessageDialog(this, "Erro: " + ex.getMessage());
      }
    } else if(ev.getSource() == btFechar) {
      setVisible(false);
      System.exit(0);
    }
  }

  public void hyperlinkUpdate(HyperlinkEvent e) {
    if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
      JEditorPane pane = (JEditorPane)e.getSource();
      if(e instanceof HTMLFrameHyperlinkEvent) {
        HTMLFrameHyperlinkEvent evt = (HTMLFrameHyperlinkEvent)e;
        HTMLDocument doc = (HTMLDocument)pane.getDocument();
        doc.processHTMLFrameHyperlinkEvent(evt);
      } else {
        try {
          pane.setPage(e.getURL());
        } catch(Throwable t) {
          t.printStackTrace();
        }
      }
    }
  }

  public static void main(String[] args) {
    new HTMLTeste();
  }
}
