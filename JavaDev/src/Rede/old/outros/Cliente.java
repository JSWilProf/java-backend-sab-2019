package Rede.old.outros;

import java.io.*;
import java.net.*;

public class Cliente {
	public static void main(String[] args) {
		// Estabelecer conexão com o servidor
		try (Socket s = new Socket("10.84.144.250", 1234);
				InputStreamReader is = new InputStreamReader(s.getInputStream());
				BufferedReader in = new BufferedReader(is);
				PrintWriter out = new PrintWriter(s.getOutputStream());) {

			// lê e envia texto de e para o servidor
			System.out.println(in.readLine());
			out.println("Teste");
			out.flush();

			String txt = in.readLine();
			System.out.println(txt);

			out.println("olá, 1234");
			out.flush();

			txt = in.readLine();
			System.out.println(txt);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
