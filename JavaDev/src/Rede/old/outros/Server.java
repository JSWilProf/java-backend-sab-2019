package Rede.old.outros;

import java.io.*;
import java.net.*;

public class Server {
	public static void main(String[] args) throws IOException {

		try (ServerSocket server = new ServerSocket(1234);) {

			Socket s = server.accept();
			InputStreamReader is = new InputStreamReader(s.getInputStream());
			BufferedReader in = new BufferedReader(is);
			PrintWriter out = new PrintWriter(s.getOutputStream());

			out.println("Bem vindo " + s.getInetAddress());
			out.flush();

			out.println("S:" + in.readLine());
			out.flush();

			s.close();
		}
	}
}
