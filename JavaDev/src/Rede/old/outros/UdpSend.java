package Rede.old.outros;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpSend {
	public static void main(String[] args) throws IOException {
		
    byte[] ary = new byte[128];

    DatagramPacket pack = new DatagramPacket(ary, 128);
		pack.setAddress(InetAddress.getByName(args[1]));
		pack.setData(args[2].getBytes());
		pack.setPort(1111);

		DatagramSocket sock = new DatagramSocket();
		sock.send(pack);
		sock.close();
		
	}
}
