package Rede.old.outros;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class TelaExemplo extends JFrame {

	private JPanel contentPane;
	private JTextField tfTxt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaExemplo frame = new TelaExemplo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaExemplo() {
		final JTextArea taTexto = new JTextArea();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 304, 366);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnInicio = new JMenu("Inicio");
		menuBar.add(mnInicio);
		
		JMenuItem mntmConectar = new JMenuItem("Conectar");
		mnInicio.add(mntmConectar);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mnInicio.add(mntmSair);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lbTxt = new JLabel("Mens.");
		lbTxt.setBounds(11, 13, 57, 15);
		
		tfTxt = new JTextField();
		tfTxt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String txt = tfTxt.getText();
				tfTxt.setText("");
				taTexto.append(txt + "\n");
			}
		});
		tfTxt.setBounds(94, 11, 187, 19);
		tfTxt.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(11, 36, 270, 220);
		
		scrollPane.setViewportView(taTexto);
		
		JButton btEnviar = new JButton("Enviar");
		btEnviar.setBounds(11, 262, 106, 25);
		btEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String txt = tfTxt.getText();
				tfTxt.setText("");
				taTexto.append(txt + "\n");
			}
		});
		btEnviar.setMnemonic('e');
		
		JButton btSair = new JButton("Sair");
		btSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btSair.setBounds(177, 262, 104, 25);
		btSair.setMnemonic('r');
		contentPane.setLayout(null);
		contentPane.add(lbTxt);
		contentPane.add(tfTxt);
		contentPane.add(scrollPane);
		contentPane.add(btEnviar);
		contentPane.add(btSair);
	}
}
