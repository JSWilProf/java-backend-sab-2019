package Rede.old.outros.cliente;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class testeJoptionPane {
	public static void main(String[] args) throws InterruptedException {
		JOptionPane alerta = new JOptionPane("Conectando...");
        JDialog dialog = alerta.createDialog("Alerta");
        dialog.setModal(false);
		dialog.setVisible(true);
		
		for (int i = 0; i < 50; i++) {
			System.out.println(i);
			if(!dialog.isActive()) {
				dialog.setVisible(true);
			}
			Thread.sleep(50);
		}
		dialog.dispose();

	}
}
