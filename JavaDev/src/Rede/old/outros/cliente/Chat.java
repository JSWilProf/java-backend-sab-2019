package Rede.old.outros.cliente;

import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JMenuItem;

public class Chat extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jLabel = null;
	private JTextField jTextField = null;
	private JScrollPane jScrollPane = null;
	private JTextArea jTextArea = null;
	private JMenuBar jJMenuBar = null;
	private JMenu jMenu = null;
	// Socket de conexão ao servidor
	private Socket skt = null;

	// Obtendo o canal de leitura do socket
	private BufferedReader in = null;

	// Obtendo o canal de gravação do socket
	private PrintWriter out = null;

	// Thread de leitura
	private Thread leitor = null;
	private JMenuItem jMenuItem = null;
	private JMenuItem jMenuItem1 = null;


	/**
	 * This method initializes jTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField() {
		if (jTextField == null) {
			jTextField = new JTextField();
			jTextField.setBounds(new Rectangle(110, 35, 161, 20));
			jTextField.setEnabled(false);
			jTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// le mensagens do campo
					String mens = jTextField.getText();
					
					// escreve no servidor
					out.println(mens);
					out.flush();
					
					// limap o campo de mensagens
					jTextField.setText("");
				}
			});
		}
		return jTextField;
	}

	/**
	 * This method initializes jScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(new Rectangle(20, 70, 311, 161));
			jScrollPane.setViewportView(getJTextArea());
			jScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	      public void adjustmentValueChanged(AdjustmentEvent e){
	        jTextArea.setCaretPosition(jTextArea.getText().length());
	      }
	    });
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getJTextArea() {
		if (jTextArea == null) {
			jTextArea = new JTextArea();
			jTextArea.setEditable(false);
		}
		return jTextArea;
	}

	/**
	 * This method initializes jJMenuBar
	 * 
	 * @return javax.swing.JMenuBar
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getJMenu());
		}
		return jJMenuBar;
	}

	/**
	 * This method initializes jMenu
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenu() {
		if (jMenu == null) {
			jMenu = new JMenu();
			jMenu.setText("Chat");
			jMenu.add(getJMenuItem());
			jMenu.add(getJMenuItem1());
		}
		return jMenu;
	}

	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem() {
		if (jMenuItem == null) {
			jMenuItem = new JMenuItem();
			jMenuItem.setText("Conectar");
			jMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					try {
						// conecta ao servidor
						skt = new Socket("192.132.1.20", 1234);

						// obtem o canal de entrada de mensagens
						in = new BufferedReader(new InputStreamReader(skt
								.getInputStream()));
						// obtem o canal de saida de mensagens
						out = new PrintWriter(skt.getOutputStream());

						// obtem o id do cliente e envia para o servidor
						String id = JOptionPane.showInputDialog("Qual o seu ID?");
						out.println(id);
						out.flush();
						
						// habilita o campo de mensagens
						jTextField.setEnabled(true);

						// habilitar o menu desconectar
						jMenuItem1.setEnabled(true);

						// desabilita o menu conectar
						jMenuItem.setEnabled(false);
						
						leitor = new Thread() {
							public void run() {
								try {
									System.out.println("inicio");
									while (!interrupted()) {
										System.out.println("t� nessa");
										// l� linha do servidor
										String linha = in.readLine();
										// mostra na tela
										jTextArea.append(linha + "\n");
									}
									System.out.println("Fudeu");
								} catch (Exception ex) {
								}
							}
						};
						leitor.start();
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null,
								"Projemas de conex�o\n" + ex.getMessage());
					}

				}
			});
		}
		return jMenuItem;
	}

	/**
	 * This method initializes jMenuItem1	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem1() {
		if (jMenuItem1 == null) {
			jMenuItem1 = new JMenuItem();
			jMenuItem1.setText("Desconectar");
			jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {

					// deshabilitar o menu desconectar
					jMenuItem1.setEnabled(false);

					// abilita o menu conectar
					jMenuItem.setEnabled(true);

					// deshabilita o campo de mensagens
					jTextField.setEnabled(false);

					// envia mensagem de finaliza��o ao servidor
					out.println("SAIR");
					out.flush();
					
					// para a tread de leitura
					leitor.interrupt();

					// fecha a conex�o com o servidor
					try {
						skt.close();
					} catch (Exception ex) {
					}
				}
			});
		}
		return jMenuItem1;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Chat thisClass = new Chat();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public Chat() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(358, 307);
		this.setJMenuBar(getJJMenuBar());
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(39, 38, 52, 16));
			jLabel.setText("Mems.");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabel, null);
			jContentPane.add(getJTextField(), null);
			jContentPane.add(getJScrollPane(), null);
		}
		return jContentPane;
	}

} // @jve:decl-index=0:visual-constraint="10,10"
