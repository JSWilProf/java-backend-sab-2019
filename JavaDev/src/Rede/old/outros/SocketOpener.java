package Rede.old.outros;

import java.io.*;
import java.net.*;

class SocketOpener implements Runnable {
  private String host;
  private int port;
  private Socket socket;

  private SocketOpener(String aHost, int aPort) {
    socket = null;
    host = aHost;
    port = aPort;
  }

  public static Socket openSocket(String aHost, int aPort, int timeout) {
    SocketOpener opener = new SocketOpener(aHost, aPort);
    Thread t = new Thread(opener);
    t.start();
    try {
      t.join(timeout);
    } catch(InterruptedException exception) {
    }
    return opener.getSocket();
  }

  public void run() {
    try {
      socket = new Socket(host, port);
    } catch(IOException exception) {
    }
  }

  private Socket getSocket() {
    return socket;
  }

  public static void main(String[] args) {
    String host;
    if(args.length > 0)
      host = args[0];
    else
      host = "www.google.com.br";

    int port;
    if(args.length > 1)
      port = Integer.parseInt(args[1]);
    else
      port = 80;

    int timeout = 5000;
    Socket s = SocketOpener.openSocket(host, port, timeout);

    if(s == null)
      System.out.println("The socket could not be opened.");
    else
      System.out.println(s);
  }
}