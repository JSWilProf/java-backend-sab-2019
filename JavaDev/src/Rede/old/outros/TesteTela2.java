package Rede.old.outros;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class TesteTela2 extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField textField;
	private JLabel lblIdade;
	private JTextField textField_1;
	private JButton btnOk;
	private JButton btnSaiur;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TesteTela2 frame = new TesteTela2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TesteTela2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 233, 188);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNome());
		contentPane.add(getTextField());
		contentPane.add(getLblIdade());
		contentPane.add(getTextField_1());
		contentPane.add(getBtnOk());
		contentPane.add(getBtnSaiur());
	}

	private JLabel getLblNome() {
		if (lblNome == null) {
			lblNome = new JLabel("Nome");
			lblNome.setBounds(12, 12, 61, 15);
		}
		return lblNome;
	}

	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setBounds(83, 10, 114, 19);
			textField.setColumns(10);
		}
		return textField;
	}

	private JLabel getLblIdade() {
		if (lblIdade == null) {
			lblIdade = new JLabel("Idade");
			lblIdade.setBounds(12, 53, 61, 15);
		}
		return lblIdade;
	}

	private JTextField getTextField_1() {
		if (textField_1 == null) {
			textField_1 = new JTextField();
			textField_1.setBounds(83, 51, 114, 19);
			textField_1.setColumns(10);
		}
		return textField_1;
	}

	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton("OK");
			btnOk.setMnemonic('o');
			btnOk.addActionListener(this);
			btnOk.setBounds(37, 122, 53, 25);
		}
		return btnOk;
	}

	private JButton getBtnSaiur() {
		if (btnSaiur == null) {
			btnSaiur = new JButton("Sair");
			btnSaiur.setMnemonic('S');
			btnSaiur.addActionListener(this);
			btnSaiur.setBounds(127, 122, 57, 25);
		}
		return btnSaiur;
	}

	public void actionPerformed(ActionEvent ev) {
		String cmd = ev.getActionCommand();
		
		if(cmd.equals("OK")) {
			
		} else {
			System.exit(0);
		}
	}
}
