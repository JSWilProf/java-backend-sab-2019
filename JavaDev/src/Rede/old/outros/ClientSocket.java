package Rede.old.outros;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class ClientSocket extends JFrame implements ActionListener {
	private JTextArea ta = new JTextArea(15, 25);
	private JTextField msg = new JTextField(15);
	private Socket skt = null;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private Thread inputThread = null;
	private boolean continua = true;

	public ClientSocket() {
		JMenuBar bar = new JMenuBar();
		setJMenuBar(bar);
		JMenu menu = new JMenu("Inicial");
		bar.add(menu);
		JMenuItem it = new JMenuItem("Conectar");
		menu.add(it);
		it.addActionListener(this);
		it = new JMenuItem("Desconectar");
		menu.add(it);
		it.addActionListener(this);

		setTitle("Chat 1/2 Boca :D");
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p.add(new JLabel("Mensagem"));
		p.add(msg);
		add(p, "North");
		msg.addActionListener(this);
		msg.setActionCommand("Enviar");
		msg.setEnabled(false);

		JScrollPane sp = new JScrollPane(ta);
		ta.setEditable(false);
		JPanel p2 = new JPanel();
		p2.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		p2.add(sp);
		add(p2, "Center");

		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		String cmd = ev.getActionCommand();
		if (cmd.equals("Enviar")) {
			out.println(msg.getText());
			out.flush();
			msg.setText("");
			msg.requestFocus();
		} else if (cmd.equals("Conectar")) {
			try {
				skt = new Socket("10.84.144.250", 1234);
				in = new BufferedReader(new InputStreamReader(
						skt.getInputStream()));
				out = new PrintWriter(skt.getOutputStream());
				msg.setEnabled(true);
				continua = true;
				
				inputThread = new Thread() {
					public void run() {
						while(continua) {
							try {
								ta.append(in.readLine() + "\n");
								int tam = ta.getText().length();
								ta.setCaretPosition(tam);
							} catch (IOException e) {
							}
						}
					}
				};
				inputThread.start();
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(this, "Problemas na Conexão");
			}
		} else {
			continua = false;
			out.println("SAIR");
			out.flush();
			try {
				skt.close();
			} catch (IOException e) {
			}
			msg.setText("");
			msg.setEnabled(false);
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ClientSocket();
			}
		});
	}
}
