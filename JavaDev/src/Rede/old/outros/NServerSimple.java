package Rede.old.outros;

import java.io.*;
import java.net.*;
import java.util.Vector;

public class NServerSimple {
	public static void main(String[] args) {
		try (
			// Configurando a porta do Servidor
			ServerSocket server = new ServerSocket(1234); ) {

			while (true) {
				// Aguardando a conexão de um cliente na porta configurada
				Socket s = server.accept();

				// Mostar o início da conexão na console do servidor
				System.out.println("Iniciando Conexão com: "
						+ s.getInetAddress());

				// Cria um cliente MultiThread e Start o processo para atender o
				// cliente
				Cliente2 c = new Cliente2(s);
				c.start();
			}
		} catch (IOException ex) {
			System.out.println("Erro durante a conexão: " + ex.getMessage());
		}
	}
}

class Cliente2 extends Thread {
	private static Vector<Cliente2> clientes = new Vector<Cliente2>();
	private Socket s = null;

	public Cliente2(Socket s) {
		this.s = s;
	}

	public void run() {
		// Adiciona o cliente á lista para a propagação das mensagens
		clientes.addElement(this);
		try (
			// Obtém o canal de leitura do cliente
			InputStreamReader is = new InputStreamReader(s.getInputStream());
			BufferedReader in = new BufferedReader(is);

			// Obtém o canal de gravação ao cliente
			OutputStreamWriter os = new OutputStreamWriter(s.getOutputStream());
			BufferedWriter saida = new BufferedWriter(os); ) {

			// Envia mensagem de boas vindas para o cliente
			saida.write("Bem vindo " + s.getInetAddress() + "\n");
			saida.flush();

			// Rotina para a conversa com o cliente
			while (!isInterrupted()) {
				while(in.ready()) {
					// Lê o cliente
					String txt = in.readLine();

					// Grava no cliente
					for (Cliente2 cli : clientes) {
						PrintWriter out = new PrintWriter(cli.s.getOutputStream());
						out.println("S: " + txt);
						out.flush();
					}

					// Se a string lida for SAIR finaliza a conversa
					if (txt.equals("SAIR")) {
						clientes.remove(this);
						break;
					}
				}
			}
			// Mostar a finalização da conexão na console do servidor
			System.out.println("Finalizando Conexão com: "
					+ s.getInetAddress());

			// Fecha a conexão com o cliente
			s.close();
		} catch (IOException ex) {
			System.out.println("Erro durante a conexão: " + ex.getMessage());
		}
	}
}