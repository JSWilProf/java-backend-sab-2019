package Rede.old.outros;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JOptionPane;

public class CGIPost {
  public static void main(String args[]) throws Exception {
	  
	  
    String fullURL = JOptionPane.showInputDialog("Informe a URL");
    
    URL u = new URL(fullURL);
    URLConnection conn = u.openConnection();
    conn.setDoOutput(true);
    OutputStream theControl = conn.getOutputStream();
    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(theControl));
    for (int i = 1; i < args.length; i++) {
      out.write(args[i] + "\n");
    }
    out.close();
    
    InputStream theData = conn.getInputStream();

    String contentType = conn.getContentType();
    if (contentType.toLowerCase().startsWith("text")) {
      BufferedReader in = new BufferedReader(new InputStreamReader(theData));
      String line;
      while ((line = in.readLine()) != null) {
        System.out.println(line);
      }
    }
  }
}
