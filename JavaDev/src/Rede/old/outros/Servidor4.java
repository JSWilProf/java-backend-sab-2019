package Rede.old.outros;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor4 {
	public static void main(String[] args) {
		try (ServerSocket srv = new ServerSocket(1234);) {
			while (true) {
				final Socket skt = srv.accept();
				Thread t = new Thread() {
					public void run() {
						try (BufferedReader in = new BufferedReader(
								new InputStreamReader(skt.getInputStream()));
								PrintWriter out = new PrintWriter(
										skt.getOutputStream());) {

							out.println("Bem Vindo!");
							out.flush();

							String msg = "";
							while (!msg.equals("SAIR")) {
								msg = in.readLine();
								out.println("S: " + msg);
								out.flush();
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				};
				t.start();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
