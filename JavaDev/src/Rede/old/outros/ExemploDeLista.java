package Rede.old.outros;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ExemploDeLista {
	private static List<Socket> lista = new ArrayList<Socket>();

	public static void main(String[] args) throws Exception {
		for (int i = 0; i < 3; i++) {
			Socket skt = new Socket("localhost", 1234);
			synchronized (lista) {
				lista.add(skt);
			}
		}

		for (int i = 0; i < 1002; i++) {
			int idx = (int) Math.floor(Math.random() * 3);
			synchronized (lista) {
				Socket s = lista.get(idx);
				PrintWriter out = new PrintWriter(s.getOutputStream());
				out.println("Cliente:" + (Math.random() * 10));
				out.flush();
			}
		}
		synchronized (lista) {
			for (int i = 0;i< lista.size();i++) {
				Socket s = lista.get(i);
				lista.remove(s);
				s.close();
			}
		}

		System.out.println(lista.size());
	}
}
