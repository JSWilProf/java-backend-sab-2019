package Rede.old.abs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import Rede.old.abs.base.AbstractHandler;
import Rede.old.abs.base.AbstractServer;

@SuppressWarnings("serial")
public class TextoServer extends AbstractServer {
  public TextoServer() {
    super("Texto Server");
  }

  public void run() {
    try {
      server = new ServerSocket(porta);

      while(true) {
        Socket s = server.accept();
        ta.append("aceitando conexão de " + s.getInetAddress() + "\n");
        new TextoClient(s).start();
      }
    } catch(IOException ex) {
    }
  }
  
  class TextoClient extends AbstractHandler {
    private BufferedReader in = null;

    public TextoClient(Socket s) throws IOException {
      super(s);
    }
    
    public void run() {
      String ip = sckt.getInetAddress().toString();
     
      try {
        PrintWriter out = new PrintWriter(sckt.getOutputStream());
        out.println("Bem vindo " + sckt.getInetAddress());
        out.flush();
        
        in = new BufferedReader(new InputStreamReader(sckt.getInputStream()));

        while(true) {
          while(in.ready()) {
          	String txt = in.readLine();
            ta.append(ip + ": " + txt + "\n");
            repaint();
            out.println("Echo: " + txt);
            out.flush();
          }
          Thread.sleep(1000);
        }
      } catch(Exception ex) {
         try {
          ta.append("conexão finalizada com " + ip + "\n");
          sckt.close();
        } catch(IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static void main(String[] args) {
    new TextoServer();
  }
}
