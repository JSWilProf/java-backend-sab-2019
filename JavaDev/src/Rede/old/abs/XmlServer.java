package Rede.old.abs;

import java.io.*;
import java.net.*;

import Rede.old.abs.base.AbstractHandler;
import Rede.old.abs.base.AbstractServer;
import Xml.Services.Attribute;
import Xml.Services.Parm;
import Xml.Services.ParseXml;

@SuppressWarnings("serial")
public class XmlServer extends AbstractServer {
  public XmlServer() {
    super("Xml Server");
  }

  public void run() {
    try {
      server = new ServerSocket(porta);

      while(true) {
        Socket s = server.accept();
        ta.append("aceitando conexão de " + s.getInetAddress() + "\n");
        new TextoClient(s).start();
      }
    } catch(IOException ex) {
    }
  }
  
  class TextoClient extends AbstractHandler {
    private InputStream in = null;

    public TextoClient(Socket s) throws IOException {
      super(s);
    }
    
    public void run() {
      String ip = sckt.getInetAddress().toString();
      
      try {
        in = new BufferedInputStream(sckt.getInputStream());
        ParseXml db = new ParseXml();
        db.setXml(in);

        if(db.processXml()) {
          Parm p = db.getParm();
        
          ta.append(ip + "\n" + printParm(p, 0) + "\n");
          repaint();
        }
      } catch(Exception ex) {
         try {
          ta.append("conexão finalizada com " + ip + "\n");
          sckt.close();
        } catch(IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  private String printParm(Parm p, int tab) {
    StringBuffer sb = new StringBuffer();
    int t = p.count();

    if(tab > 0) for(int i = 0;i < tab;i++)
      sb.append("  ");

    sb.append("Total: " + t + " Name: " + p.getName());
    String v = p.getValue();

    if(v != null)
      sb.append("=" + v + "\n");
    else
      sb.append("\n");

    java.util.List<Attribute> attrs = p.getAttributes();

    if(attrs != null) {
      int a = attrs.size();

      if(a > 0) {
        if(tab > 0) for(int i = 0;i < tab;i++)
          sb.append("  ");

        sb.append(" Attrs:");

        for(int i = 0;i < a;i++) {
          if(i != 0) {
            sb.append("       ");

            if(tab > 0) for(int j = 0;j < tab;j++)
              sb.append("  ");
          }

          Attribute attr = attrs.get(i);
          sb.append(" " + attr.getName() + "=" + attr.getValue() + "\n");
        }
      }
    }

    for(int i = 0;i < t;i++) {
      Parm pp = p.get(i);
      sb.append(printParm(pp, tab + 1) + "\n");
    }
    
    return sb.toString();
  }

  public static void main(String[] args) {
    new XmlServer();
  }
}
