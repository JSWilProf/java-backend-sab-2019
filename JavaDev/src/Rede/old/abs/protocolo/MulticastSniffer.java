package Rede.old.abs.protocolo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastSniffer {

	public static void main(String[] args) throws IOException {

		byte[] buffer = new byte[65509];
		DatagramPacket pkt = new DatagramPacket(buffer, buffer.length);
		try (MulticastSocket mSkt = new MulticastSocket(1234);) {
			mSkt.joinGroup(InetAddress.getByName("224.0.0.1"));

			while (true) {
				mSkt.receive(pkt);
				System.out
						.println(new String(pkt.getData(), 0, pkt.getLength()));
			}
		}
	}
}