package Rede.old.abs.protocolo;

import java.nio.charset.Charset;

public class DisplayAvaliableCharSets {
	public static void main(String[] args) {
		for (Charset txt : Charset.availableCharsets().values()) {
			System.out.println(txt.displayName());
		}
	}
}
