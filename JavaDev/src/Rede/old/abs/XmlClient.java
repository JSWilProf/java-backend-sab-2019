package Rede.old.abs;

import java.io.*;
import java.net.*;

import javax.swing.*;

public class XmlClient {
  public static void main(String[] args) {
    try {
      JFileChooser chooser = new JFileChooser();

      if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        File xmlFile = chooser.getSelectedFile();

        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(xmlFile))); 

        Socket s = new Socket("localhost", 1234);
        OutputStream out = s.getOutputStream();

        for(String linha;(linha = in.readLine()) != null;) {
          System.out.println(linha + "\n");
          out.write(linha.trim().getBytes());
        }

        out.flush();
        out.close();
        in.close();
        s.close();
      }
    } catch(FileNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "Arquivo não encontrado!\n"
        + ex.getMessage());
    } catch(IOException ex) {
      JOptionPane.showMessageDialog(null,
        "Erro de Leitura/Gravação!\n" + ex.getMessage());
      ex.printStackTrace();
    }
  }
}
