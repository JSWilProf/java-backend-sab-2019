<?xml version="1.0"?>
<html xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xsl:version="1.0">
	<head>
		<title>Meu XML</title>
	</head>
	<body>
		<center>
			<h2>Meu XML</h2>
			<table border="1">
				<tr align="center">
					<th><b>Nome</b></th>
					<th colspan="3"><b>Endereco</b></th>
					<th colspan="3"><b>Telefone</b></th>
				</tr>
				<xsl:for-each select="/pessoas/pessoa">
					<tr align="right">
						<td><xsl:value-of select="nome" /></td>
						<td><xsl:value-of select="endereco/logradouro" /></td>
						<td><xsl:value-of select="endereco/numero" /></td>
						<td><xsl:value-of select="endereco/bairro" /></td>
						<td><table border="1">
						<xsl:for-each select="telefone">
							<tr>
							<td><xsl:value-of select="@tipo" /></td>
							<td><xsl:value-of select="ddd" /></td>
							<td><xsl:value-of select="numero" /></td>
							</tr>
						</xsl:for-each>
						</table></td>
					</tr>
				</xsl:for-each>
			</table>
		</center>
	</body>
</html>
