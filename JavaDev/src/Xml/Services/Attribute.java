// --------------------------------------------------------
//
//  JavaBean: Atribute
//
//  Objetivo: Representa um atributo XML (nome e valor)
//
//
// Autor: Wilson José de Santana
// Data: Março-2003
// Atualização: Junho-2006 
// Motivo: Compatilização com Java 5
//
// --------------------------------------------------------
package Xml.Services;

import java.lang.String;

public class Attribute {
  private String name;
  private String value;

  public Attribute() {
    name = null;
    value = null;
  }

  public Attribute(String name, String value) {
    setName(name);
    setValue(value);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
  
  public String toString() {
    return name + "=\"" + value + "\" ";
  }
}
