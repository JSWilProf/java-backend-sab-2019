// --------------------------------------------------------
//
//  JavaBean: ParseXml
//
//  Objetivo: Obter os dados de umarquivo XML e 
//            retornando um Objeto Vector com
//            os dados consultados
//
//  Propriedades:
//       String getXml()     : Obtém a URI XML a ser parseado
//       void setXml(String) : Define a URI XML a ser parseado
//       Parm getParm()      : Obtém o Objeto Parm que representa
//                             todos os nós do codumento XML processado
//       boolean processXml(): Processa o documento XML
//
//
// Autor: Wilson José de Santana
// Data: Março-2003
// Atualização: Junho-2006 
// Motivo: Compatilização com Java 5
//
// --------------------------------------------------------
package Xml.Services;

// Imported java.io classes
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStream;

// Imported SAX classes
import org.xml.sax.SAXException;

// Imported DOM classes
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

// Imported JAVA API for XML Parsing classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ParseXml {
  private String xml;
  private InputStream xmlIs;
  private Document xmlDoc;
  private Parm parm = null;
  private boolean nameSet = false;

  public static void main(String[] args) throws Exception {
    ParseXml db = new ParseXml();
    db.setXml(args[0]);

    if(db.processXml()) {
      Parm p = db.getParm();

      System.out.println("[" + p.getName() + "]");
      db.printParm(p, 0);
    }
  }

  private void printParm(Parm p, int tab) {
    int t = p.count();

    if(tab > 0) for(int i = 0;i < tab;i++)
      System.out.print("  ");

    System.out.print("Total: " + t + " Name: " + p.getName());
    String v = p.getValue();

    if(v != null)
      System.out.println("=" + v);
    else
      System.out.println();

    java.util.List<Attribute> attrs = p.getAttributes();

    if(attrs != null) {
      int a = attrs.size();

      if(a > 0) {
        if(tab > 0) for(int i = 0;i < tab;i++)
          System.out.print("  ");

        System.out.print(" Attrs:");

        for(int i = 0;i < a;i++) {
          if(i != 0) {
            System.out.print("       ");

            if(tab > 0) for(int j = 0;j < tab;j++)
              System.out.print("  ");
          }

          Attribute attr = attrs.get(i);
          System.out.println(" " + attr.getName() + "=" + attr.getValue());
        }
      }
    }

    for(int i = 0;i < t;i++) {
      Parm pp = p.get(i);
      printParm(pp, tab + 1);
    }
  }

  public Parm getParm() {
    return parm;
  }

  public boolean processXml() throws FileNotFoundException,
      ParserConfigurationException, SAXException, IOException {
    if(xml == null && xmlIs == null)
      return false;
    else {
      DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
      dFactory.setNamespaceAware(true);
      dFactory.setValidating(true);

      DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
      
      if(xml == null)
        xmlDoc = dBuilder.parse(xmlIs);
      else
        xmlDoc = dBuilder.parse(xml);

      parm = processXML(parm, xmlDoc);

      return (parm != null ? true : false);
    }
  }

  private Parm processXML(Parm p, Node node) {
    Parm xparm;

    if(node == null) return null;

    short type = node.getNodeType();

    switch(type) {
      case Node.DOCUMENT_NODE: {
        nameSet = false;
        Document document = (Document)node;

        p = processXML(p, document.getDocumentElement());
        break;
      }

      case Node.DOCUMENT_TYPE_NODE:
        break;

      case Node.ELEMENT_NODE: {
        Node child = node.getFirstChild();

        xparm = new Parm();

        if(p != null) p.add(xparm);

        p = xparm;

        p.setName(node.getNodeName());

        if(node.getParentNode().getNodeType() != Node.DOCUMENT_NODE) {
          nameSet = true;
        }

        Attr[] attrs = sortAttributes(node.getAttributes());

        if(attrs.length > 0) {
          java.util.List<Attribute> myAttrs = new java.util.ArrayList<Attribute>(attrs.length);
          
          for(Attr attr: attrs) 
            myAttrs.add(new Attribute(attr.getNodeName(), attr.getNodeValue()));

          p.setAttributes(myAttrs);
        }

        while(child != null) {
          processXML(p, child);
          child = child.getNextSibling();
        }

        break;
      }

      case Node.ENTITY_REFERENCE_NODE: {
        Node child = node.getFirstChild();

        while(child != null) {
          processXML(p, child);
          child = child.getNextSibling();
        }

        break;
      }

      case Node.CDATA_SECTION_NODE:
        break;

      case Node.TEXT_NODE: {
        if(nameSet) {
          String value = node.getNodeValue();

          if(value.matches("[\\p{Alnum}\\p{Punct}]+?")) {
            p.setValue(node.getNodeValue());
          }

          nameSet = false;
        }

        break;
      }

      case Node.PROCESSING_INSTRUCTION_NODE:
        break;
    }

    return p;
  }

  public void setXml(String x) {
    xml = x;
    xmlIs = null;
  }

  public void setXml(InputStream x) {
    xml = null;
    xmlIs = x;
  }

  public String getXml() {
    return xml;
  }

  private Attr[] sortAttributes(NamedNodeMap attrs) {

    int len = (attrs != null) ? attrs.getLength() : 0;
    Attr array[] = new Attr[len];
    for(int i = 0;i < len;i++) 
      array[i] = (Attr)attrs.item(i);
    
    for(int i = 0;i < len - 1;i++) {
      String name = array[i].getNodeName();
      int index = i;

      for(int j = i + 1;j < len;j++) {
        String curName = array[j].getNodeName();
        if(curName.compareTo(name) < 0) {
          name = curName;
          index = j;
        }
      }

      if(index != i) {
        Attr temp = array[i];
        array[i] = array[index];
        array[index] = temp;
      }
    }

    return array;
  }
}
