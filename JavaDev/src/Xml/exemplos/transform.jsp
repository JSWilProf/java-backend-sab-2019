<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml"  prefix="x" %>

<c:import url="/WEB-INF/hobby.xml" var="buildXml" />
<c:import url="/WEB-INF/hobby.xsl" var="xslt" />
<x:transform xml="${buildXml}" xslt="${xslt}" />
