//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2017.09.25 às 08:57:55 PM BRT 
//


package Xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "aluno"
})
@XmlRootElement(name = "salaDeAula")
public class SalaDeAula {

    @XmlElement(required = true)
    protected List<Aluno> aluno;

    /**
     * Gets the value of the aluno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aluno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAluno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Aluno }
     * 
     * 
     */
    public List<Aluno> getAluno() {
        if (aluno == null) {
            aluno = new ArrayList<Aluno>();
        }
        return this.aluno;
    }

}
