// --------------------------------------------------------
//
//  JavaBean: Parm
//
//  Objetivo: Representar a estrutura de uma árvore ou nó XML
//           
//  Propriedades:
//       String getName()                : Obtém o nome do nó
//       setName(String)                 : Atribui o nome do nó
//       String getValue()               : Obtém o valor do nó
//       setValue(String)                : Atribui o valor do nó
//       List<Attribute> getAttributes() : Obtém uma lista dos atributos
//                                         deste nó
//       setAttributes(List<Attribute>)  : Atribui uma lista de atributos
//                                         para este nó
//       Parm[] get(String)              : Obtém um array dos sub-nós 
//                                         com o nome informado
//       Parm get(int)                   : Obtém o sub-nó contido neste nó 
//       add(Parm)                       : Adiciona um sub-nó neste nó 
//       int count()                     : Informa o número de nós contidos
//                                         neste nó
//
// Autor: Wilson José de Santana
// Data: Março-2003
// Atualização: Junho-2006 
// Motivo: Compatilização com Java 5
//
// --------------------------------------------------------
package Xml.old.Services;

import java.lang.String;
import java.util.List;
import java.util.ArrayList;

public class Parm {
  private String name;
  private String value;
  private List<Attribute> attributes = new ArrayList<Attribute>();
  private List<Parm> parms = new ArrayList<Parm>();

  public Parm() {
    name = null;
    value = null;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Attribute getAttribute(String name) {
	  Attribute att = null;
	  for (Attribute a : attributes) {
		if(a.getName().equals(name)) {
			att = a;
			break;
		}
	}
	 return att;
  }
  
  public List<Attribute> getAttributes() {
    return attributes;
  }

  public void setAttributes(List<Attribute> attr) {
    this.attributes = attr;
  }

  public int count() {
    if(parms == null)
      return 0;
    else
      return parms.size();
  }

  public List<Parm> get(String name) {
    List<Parm> arr = new ArrayList<Parm>();

    if(parms == null)
      return null;
    else {
      for(Parm a: parms) {
        if(a.getName().equals(name)) 
          arr.add(a);
      }

      if(arr.size() == 0)
        return null;
      else
        return arr;
    }
  }

  public Parm get(int i) {
    if(parms == null)
      return null;
    else
      return parms.get(i);
  }

  public void add(Parm parm) {
    parms.add(parm);
  }
}
