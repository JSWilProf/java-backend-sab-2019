package Xml.old.TesteXml;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JavaToXmlBindExample {
	public static void main(String[] args) {
		try {
			// Configuração da classe para a leitura/gravação de XML
			JAXBContext ctx = JAXBContext.newInstance(Pessoas.class);
			// Lê XML e grama Objetos Java
			Unmarshaller u = ctx.createUnmarshaller();
			Pessoas pessoas = (Pessoas) u.unmarshal(
					new File("src/TesteXml/hobby.xml"));

			// Executa um processamento qualquer com os objetos Java
			for (int i = 0; i < pessoas.getPessoa().size(); i++) {
				Pessoa p = pessoas.getPessoa().get(i);
				System.out.println("Nome: " + p.getNome().getPrenome() + " "
						+ p.getNome().getSobrenome() + "\nIdade: "
						+ p.getIdade() + "\nHobby: " + p.getHobby() + "\n");
			}
	
			// Lê Objeto Java e Grava XML
			Marshaller m = ctx.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(pessoas, System.out);
			
		} catch (JAXBException ex) {
			System.out.println("JAXB Binding Exception");
			ex.printStackTrace();
		}
	}
}
