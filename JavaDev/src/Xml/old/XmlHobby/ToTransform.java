package Xml.old.XmlHobby;

import java.net.URL;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class ToTransform {
  public static void main(String[] args) {
    try {
      URL xml = Xml.old.XmlHobby.Pessoas.class.getResource("hobby.xml");
      URL xsl = Xml.old.XmlHobby.Pessoas.class.getResource("hobby.xsl");

      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer t = factory.newTransformer(new StreamSource(xsl.openStream()));
      t.transform(new StreamSource(xml.openStream()), new StreamResult(System.out));
    }
    catch(Exception ex) {
      System.out.println("JAXB Binding Exception");
      ex.printStackTrace();
    }
  }
}
