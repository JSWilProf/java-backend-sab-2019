package Xml.old.XmlHobby;

import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class ListaContatos {
  public static void main(String[] args) {
    try {
      JAXBContext ctx = JAXBContext.newInstance(Pessoas.class);
      Unmarshaller um = ctx.createUnmarshaller();
      URL xml = Xml.old.XmlHobby.ListaContatos.class.getResource("hobby.xml");
      Pessoas c = (Pessoas)um.unmarshal(xml);
      
      for(Pessoa obj : c.getPessoa()) {
        System.out.format("Nome: %s %s\nIdade: %s\nHobby: %s\n", 
          obj.getNome().getPrenome(), 
          obj.getNome().getSobrenome(),
          obj.getIdade(), obj.getHobby());
        
        for(Profissao prof : obj.getProfissao()) {
          System.out.format("\tProffis�o: %s%n", prof.getvalue());
        }
        System.out.println("---------------------------------------------");
      }
      
      Marshaller m = ctx.createMarshaller();
      m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      m.setProperty(Marshaller.JAXB_ENCODING, "iso-8859-1");
      m.marshal(c, System.out);
    } catch(JAXBException ex) {
      System.out.println(ex.getMessage());
    }
  }
}
