package Xml.old.XmlHobby;

import java.net.URL;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class JavaXmlBindToTransform {
	public static void main(String[] args) {
		try {
			URL xml = Pessoas.class.getResource("hobby.xml");
			URL xsl = Pessoas.class.getResource("hobby.xsl");
			System.out.println(xml + "\n" + xsl);
			
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer t = factory	.newTransformer(new StreamSource(xsl.toURI().toString()));
			t.transform(new StreamSource(xml.toURI().toString()), new StreamResult(System.out));
		} catch (Exception ex) {
			System.out.println("JAXB Binding Exception");
			ex.printStackTrace();
		}
	}
}
