<?xml version="1.0"?>
<html xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xsl:version="1.0">
<head>
<title>Meus contatos</title>
</head>
<body>
<table border="1">
  <xsl:for-each select="/contatos/contato">
   <tr>
    <td><xsl:value-of select="nome"/></td>
    <td><xsl:value-of select="telefone/@ddd"/></td>
    <td><xsl:value-of select="telefone/@numero"/></td>
    <td><xsl:value-of select="telefone/@ramal"/></td>
   </tr>
  </xsl:for-each>
</table>
</body>
</html>