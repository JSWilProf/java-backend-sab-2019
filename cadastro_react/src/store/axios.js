import axios from 'axios';

const instance = axios.create({
    baseURL: window._env_.BASE_URL 
        ? window._env_.BASE_URL
        : 'http://localhost:8080/cadastro_boot/api'
});

instance.defaults.headers.post['Content-Type'] = 'application/json';

export const setupToken = (token) => {
    instance.defaults.headers.common['Authorization'] = 'Bearer ' + token;
};

export const resetToken = () => {
    instance.defaults.headers.common['Authorization'] = '';
};

export default instance;