import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

class Dialog extends Component {
    state = {
        show: false
    }
 
    handleClose = () => this.setState({ show: false });

    componentDidMount() {
        this.props.onRef(this)
    }
    
    componentWillUnmount() {
        this.props.onRef(undefined)
    }
    
    render() {
        return (
            <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{this.props.titulo}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.props.mensagem}</Modal.Body>
            <Modal.Footer>
                <Button variant="danger" className="shadow border border-light" 
                    onClick={() => {
                        this.handleClose();
                        this.props.handleOk();
                    }}>
                    Sim
                </Button>
                <Button variant="success" className="shadow border border-light" 
                    onClick={this.handleClose}>
                    Não
                </Button>
            </Modal.Footer>
            </Modal>
        );
    }
}

export default Dialog;