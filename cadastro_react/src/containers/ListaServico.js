import React, { Component } from 'react';
import { Table, Col, Form, Alert } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import axios from '../store/axios';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Dialog from '../components/Dialog';

class ListaServico extends Component {
    state = {
        showAlert: false,
        tipoAlerta: '',
        mensagem: '',
        servicos: [],
        id: -1,
        isSubmitting: false
    }

    handleSubmit() {
        this.setState({ isSubmitting: true });

        axios.post('/removeServico/' + this.state.id, 
        ).then(response => {
            this.setState({
                showAlert: true,
                tipoAlerta: "success",
                mensagem: 'Serviço removido com Sucesso',
                id: -1,
                servicos: [],
                isSubmitting: false
            });
            this.carregaServicos();
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: "danger",
                mensagem: reason.response.status === 403
                    ? 'Sem autorização para remover o Serviço'
                    : 'Falha ao remover o Serviço',
                isSubmitting: false
            });
        });
    }

    handleDismiss = () => this.setState({ showAlert: false });

    showDialog(idServico) {
        this.setState({ id: idServico });
        this.dialogo.setState({
            show: true
        });
    }

    carregaServicos() {
        axios.get('/listaServico'
        ).then(response => {
            this.setState({
                servicos: response.data
            });
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: "danger",
                mensagem: reason.response.status === 403
                    ? 'Sem autorização para carregar a lista de Serviços'
                    : 'Falha ao carregar a lista de Serviços'
            });
        });    
    }

    componentWillMount() {
        this.carregaServicos();
    }

    render() {
        const detalhes = this.state.servicos.map(servico => {
            return <Detalhe
                key={servico.idServico}
                role={this.props.role}
                nome={servico.nome}
                onClick={this.showDialog.bind(this, servico.idServico)}/>
        })
    
        return (
            <div>
                <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible show={this.state.showAlert}>
                    <strong>{this.state.mensagem}</strong>
                </Alert>
                <Dialog
                    onRef={ref => (this.dialogo = ref)}
                    show={false}
                    titulo="Atenção"
                    mensagem="Confirma a exclusão do Serviço?"
                    handleOk={this.handleSubmit.bind(this)}/>
                <Col className="mb-4 shadow p-4 quadro">
                    <Form noValidate>
                        <div className="d-flex justify-content-center">
                            <fieldset style={{ flex: 1 }}>
                                <div>
                                    <h2>Lista de Serviços</h2>
                                </div>
                                <div hidden={this.state.servicos.length === 0}>
                                    <Table striped hover className='table-sm'>
                                        <thead className="table-dark">
                                            <tr>
                                                { this.props.role !== ''
                                                  ? <th>Del</th>
                                                  : null
                                                }
                                                <th>Nome</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {detalhes}
                                        </tbody>
                                    </Table>
                                    <div>
                                        <Link className="btn btn-primary shadow ml-4" to="/" 
                                            disabled={this.state.isSubmitting}>Voltar</Link>
                                    </div>
                                </div>
                                <div hidden={this.state.servicos.length > 0}>
                                    <h5>Não existem <em>Serviços</em> cadastrados.</h5>
                                    <div>
                                        <Link className="btn btn-primary shadow" to="/">Voltar</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </Form>
                </Col>
            </div>
        );
    }
}

class Detalhe extends Component {
    render() {
        return(
            <tr>
                { this.props.role !== ''
                  ? <td>
                        <div onClick={this.props.onClick}>
                            <span className="btn btn-light">
                                <FontAwesomeIcon icon={faTrash} />
                            </span>
                        </div>
                    </td>
                  : null
                }  
                <td>{this.props.nome}</td>
            </tr>
        );
    }
}

const mapStateToProps = state => {
    return {
        role: state.role
    };
};

export default connect(mapStateToProps)(ListaServico);