import React, { Component } from 'react';
import { Col, Form, Button, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import * as yup from 'yup';
import axios from '../store/axios';
import { Link } from 'react-router-dom';

class CadUsuario extends Component {
    state = {
        showAlert: false,
        tipoAlerta: '',
        mensagem: '',
        idUsuario: this.props.match.params.id,
        usuario: {
            nome: '',
            old_nome: '',
            senha: '',
            administrador: false,
            habilitado: true
        }
    }

    componentDidMount() {
        if(this.state.idUsuario) {
            axios.get('/editaUsuario/' + this.state.idUsuario
            ).then(response => {
                this.setState({
                    usuario: {
                        nome: response.data.nome,
                        old_nome: response.data.old_nome,
                        senha: response.data.senha,
                        administrador: response.data.administrador,
                        habilitado: response.data.habilitado
                    }
                });
            }).catch(reason => {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "danger",
                    mensagem: reason.response.status === 403
                        ? 'Sem autorização para localizar o Usuário'
                        : 'Falha ao localizar o Usuário'
                });
            });    
        }
    }

    handleSubmit(values, {resetForm, setErrors, setSubmitting}) {
         axios.post('/salvaUsuario', {
            idUsuario: this.state.idUsuario,
            nome: values.nome,
            old_nome: this.state.usuario.old_nome,
            senha: values.senha,
            administrador: values.administrador,
            habilitado: values.habilitado
        }).then(response => {
            if(this.state.idUsuario) {
                this.props.history.push('/lstusuario');
            } else {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "success",
                    mensagem: 'Cadastrado com Sucesso',
                });
                resetForm();
                setSubmitting(false); 
            }
        }).catch(reason => {
            if(reason.response.status === 422) {
                setErrors(reason.response.data)   
            } else {
                this.setState({
                    showAlert: true,
                    tipoAlerta: "danger",
                    mensagem: reason.response.status === 403
                        ? 'Sem autorização para efetuar o Cadastro'
                        : 'Falha ao efetuar o Cadastro'
                });
            }
            setSubmitting(false); 
        });
    }

    handleDismiss = () => this.setState({ showAlert: false });

    render() {
        const schema = this.state.idUsuario
            ? yup.object().shape({
                nome: yup.string()
                    .min(5, 'O nome deve ter mais que 4 caracteres')
                    .max(15, 'Não entre com mais de 15 caracteres')
                    .required('O nome deve ser informado')
                })
            : yup.object().shape({
                nome: yup.string()
                    .min(5, 'O nome deve ter mais que 4 caracteres')
                    .max(15, 'Não entre com mais de 15 caracteres')
                    .required('O nome deve ser informado'),
                senha: yup.string()
                    .min(8, 'A senha deve ter mais que 7 caracteres')
                    .max(15, 'Não entre com mais de 15 caracteres')
                    .required('A Senha deve ser informado')
            });

        return (
            <Formik
                enableReinitialize
                validationSchema={schema}
                onSubmit={this.handleSubmit.bind(this)}
                initialValues={this.state.usuario}>
                {({handleSubmit, handleChange, values, touched, errors, isSubmitting}) => (
                    <div>
                        <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible 
                            show={this.state.showAlert}>
                            <strong>{this.state.mensagem}</strong>
                        </Alert>
                        <Col className="mb-4 shadow p-4 quadro">
                            <Form noValidate onSubmit={handleSubmit}>
                                <div className="d-flex justify-content-center">
                                    <fieldset style={{flex: 1}}>
                                        <div>
                                        {this.state.idUsuario 
                                             ? <h2>Alterar Usuário</h2>
                                             : <h2>Cadastra Usuário</h2>
                                            }
                                        </div>

                                        <Form.Group controlId="v01">
                                            <Form.Label>Nome</Form.Label>
                                            <Form.Control type="text" placeholder="Informe o Nome"
                                                name="nome" value={values.nome} onChange={handleChange}
                                                isInvalid={touched.nome && errors.nome}/>
                                            <Form.Control.Feedback type="invalid">{errors.nome}</Form.Control.Feedback>
                                        </Form.Group>
                                        { this.state.idUsuario
                                            ? null
                                            : <Form.Group controlId="v02" >
                                                <Form.Label>Senha</Form.Label>
                                                <Form.Control type="password" placeholder="Informe a Senha"
                                                    name="senha" value={values.senha} onChange={handleChange}
                                                    isInvalid={touched.nome && errors.senha} />
                                                <Form.Control.Feedback type="invalid">{errors.senha}</Form.Control.Feedback>
                                              </Form.Group> 
                                        }
                                        <Form.Group controlId="v03">
                                            <Form.Label>Administrador</Form.Label>
                                            <Form.Check type="checkbox" checked={values.administrador}
                                                name="administrador" value={values.administrador} onChange={handleChange} />
                                        </Form.Group>
                                        <Form.Group controlId="v04">
                                            <Form.Label>Habilitado</Form.Label>
                                            <Form.Check type="checkbox" checked={values.habilitado}
                                                name="habilitado" value={values.habilitado} onChange={handleChange} />
                                        </Form.Group>
                                        <div>
                                            <Button type="submit" value="primary" disabled={isSubmitting}
                                                className="shadow border border-light">Enviar</Button>
                                            { this.state.idUsuario 
                                                ? <Link className="btn btn-secondary shadow ml-4" to="/lstusuario" >Voltar</Link>
                                                : null
                                            }
                                        </div>
                                    </fieldset>
                                </div>
                            </Form>
                        </Col>
                    </div>
                )}
            </Formik>
        );
    }
}

export default CadUsuario;