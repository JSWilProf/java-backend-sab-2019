import React, { Component } from 'react';
import { Table, Col, Form, Alert } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPen, faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'
import axios from '../store/axios';
import { Link } from 'react-router-dom';

import Dialog from '../components/Dialog';

class ListaUsuario extends Component {
    state = {
        showAlert: false,
        tipoAlerta: '',
        mensagem: '',
        usuarios: [],
        id: '',
        isSubmitting: false
    }

    handleSubmit() {
        this.setState({ isSubmitting: true });

        axios.post('/removeUsuario/' + this.state.id, 
        ).then(response => {
            this.setState({
                showAlert: true,
                tipoAlerta: "success",
                mensagem: 'Usuário removido com Sucesso',
                id: '',
                usuarios: [],
                isSubmitting: false
            });
            this.carregaUsuarios();
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: "danger",
                mensagem: 'Falha ao remover o Usuário',
                isSubmitting: false
            });
        });
    }
 
    handleDismiss = () => this.setState({ showAlert: false });

    carregaUsuarios() {
        axios.get('/listaUsuario'
        ).then(response => {
            this.setState({
                usuarios: response.data
            });
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: "danger",
                mensagem: 'Falha ao carregar a lista de Usuários'
            });
        });    
    }

    showDialog(idUsuario) {
        this.setState({ id: idUsuario });
        this.dialogo.setState({
            show: true
        });
    }

    componentWillMount() {
        this.carregaUsuarios();
    }

    render() {
        const detalhes = this.state.usuarios.map(usuario => {
            return <Detalhe
                key={usuario.nome}
                idUsuario={usuario.nome}
                nome={usuario.nome}
                administrador={usuario.administrador}
                onClick={this.showDialog.bind(this, usuario.nome)}/>
        })
    
        return (
            <div>
                <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible show={this.state.showAlert}>
                    <strong>{this.state.mensagem}</strong>
                </Alert>
                <Dialog
                    onRef={ref => (this.dialogo = ref)}
                    show={false}
                    titulo="Atenção"
                    mensagem="Confirma a exclusão do Usuário?"
                    handleOk={this.handleSubmit.bind(this)}/>
                <Col className="mb-4 shadow p-4 quadro">
                    <Form noValidate>
                        <div className="d-flex justify-content-center">
                            <fieldset style={{ flex: 1 }}>
                                <div>
                                    <h2>Lista de Usuários</h2>
                                </div>
                                <div hidden={this.state.usuarios.length === 0}>
                                    <Table striped hover className='table-sm'>
                                        <thead className="table-dark">
                                            <tr>
                                                <th>Del</th>
                                                <th>Editar</th>
                                                <th>Nome</th>
                                                <th>Administrador</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {detalhes}
                                        </tbody>
                                    </Table>
                                    <div>
                                        <Link className="btn btn-primary shadow ml-4" to="/" 
                                            disabled={this.state.isSubmitting}>Voltar</Link>
                                    </div>
                                </div>
                                <div hidden={this.state.usuarios.length > 0}>
                                    <h5>Não existem <em>Usuários</em> cadastrados.</h5>
                                    <div>
                                        <Link className="btn btn-primary shadow" to="/">Voltar</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </Form>
                </Col>
            </div>
        );
    }
}

class Detalhe extends Component {
    render() {
        return(
            <tr>
                <td>
                    <div onClick={this.props.onClick}>
                        <span className="btn btn-light">
                            <FontAwesomeIcon icon={faTrash} />
                        </span>
                    </div>
                </td>
                <td>
                    <Link to={'cadusuario/' + this.props.idUsuario}>
                        <span className="btn btn-light">
                            <FontAwesomeIcon icon={faPen} />
                        </span>
                    </Link>
                </td>
                <td>{this.props.nome}</td>
                <td>
                    { this.props.administrador
                        ? <span className="btn btn-light">
                            <FontAwesomeIcon icon={faCheck} />
                          </span>
                        : <span className="btn btn-light">
                            <FontAwesomeIcon icon={faTimes} />
                          </span>
                    }
                </td>
            </tr>
        );
    }
}

export default ListaUsuario;