import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

class Home extends Component {
    render() {
        return (
            <Col className="mb-4 shadow p-4 quadro">
                <div className="d-flex justify-content-center">
                    <div>
                        <h2>Fasto Net</h2>
                    
                        <h5>
                            <em>Fasto</em> o seu <em>Provedor</em> de <em>Acesso</em>.
                        </h5>
                    </div>
    			</div>
            </Col>
        );
    }
}

export default Home;