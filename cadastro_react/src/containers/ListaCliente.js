import React, { Component } from 'react';
import { Table, Col, Form, Button, Alert } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faPen } from '@fortawesome/free-solid-svg-icons'
import axios from '../store/axios';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


import Dialog from '../components/Dialog';
import DialogoServicos from '../components/DialogoServicos';

class ListaCliente extends Component {
    state = {
        showAlert: false,
        tipoAlerta: '',
        mensagem: '',
        clientes: [],
        ids: [],
        isSubmitting: false
    }

    onChangeDelete(idCliente) {
        const index = this.state.ids.length > 0
            ? this.state.ids.findIndex(id => id === idCliente)
            : -1;
        const novoIds = index >= 0 
            ? this.state.ids.filter(id => id !== idCliente)
            : [ ...this.state.ids, idCliente ];
        this.setState({ids: novoIds});
    }

    handleSubmit() {
        this.setState({ isSubmitting: true });

        axios.post('/removeCliente', 
            [ ...this.state.ids ]
        ).then(response => {
            this.setState({
                showAlert: true,
                tipoAlerta: "success",
                mensagem: 'Cliente(s) removido(s) com Sucesso',
                ids: [],
                clientes: [],
                isSubmitting: false
            });
            this.carregaClientes();
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: "danger",
                mensagem: reason.response.status === 403
                    ? 'Sem autorização para remover o(s) Cliente(s)'
                    : 'Falha ao remover o(s) Cliente(s)',
                isSubmitting: false
            });
        });
    }
 
    handleDismiss = () => this.setState({ showAlert: false });

    handleMessage(tipo, mensagem) {
        this.setState({
            showAlert: true,
            tipoAlerta: tipo,
            mensagem: mensagem,
        });
    }

    carregaClientes() {
        axios.get('/listaCliente'
        ).then(response => {
            this.setState({
                clientes: response.data
            });
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: "danger",
                mensagem: reason.response.status === 403
                    ? 'Sem autorização para carregar a lista de Clientes'
                    : 'Falha ao carregar a lista de Clientes'
            });
        });    
    }

    showDialog() {
        this.dialogo.setState({
            show: true
        });
    }

    showDialogoServicos(idCliente) {
        axios.get('/carregaServicos/' + idCliente
        ).then(response => {
            this.dialogoServicos.setState({
                show: true,
                idCliente: idCliente,
                servicos: response.data
            });
        }).catch(reason => {
            this.setState({
                showAlert: true,
                tipoAlerta: 'danger',
                mensagem: reason.response.status === 403
                    ? 'Sem autorização para carregar a lista de Serviços'
                    : 'Falha ao carregar a lista de Serviços'
            });
        });    
    }

    componentWillMount() {
        this.carregaClientes();
    }

    render() {
        const detalhes = this.state.clientes.map(cliente => {
            return <Detalhe
                key={cliente.idCliente}
                role={this.props.role}
                idCliente={cliente.idCliente}
                nome={cliente.nome}
                email={cliente.email}
                desativado={cliente.desativado} 
                onChange={this.onChangeDelete.bind(this, cliente.idCliente)}
                onClick={this.showDialogoServicos.bind(this, cliente.idCliente)}/>
        })
    
        return (
            <div>
                <Alert variant={this.state.tipoAlerta} onClose={this.handleDismiss} dismissible 
                    show={this.state.showAlert}>
                    <strong>{this.state.mensagem}</strong>
                </Alert>
                <Dialog
                    onRef={ref => (this.dialogo = ref)}
                    show={false}
                    titulo="Atenção"
                    mensagem="Confirma a exclusão do(s) Cliente(s)?"
                    handleOk={this.handleSubmit.bind(this)}/>
                <DialogoServicos 
                    onRef={ref => (this.dialogoServicos = ref)}
                    handleMessage={this.handleMessage.bind(this)}/>
                <Col className="mb-4 shadow p-4 quadro">
                    <Form noValidate>
                        <div className="d-flex justify-content-center">
                            <fieldset style={{ flex: 1 }}>
                                <div>
                                    <h2>Lista de Clientes</h2>
                                </div>
                                <div hidden={this.state.clientes.length === 0}>
                                    <Table striped hover className='table-sm'>
                                        <thead className="table-dark">
                                            { this.props.role !== ''
                                              ? <tr>
                                                   <th>Del</th>
                                                    <th>Editar</th>
                                                    <th>Nome</th>
                                                    <th>E-Mail</th>
                                                    <th className="text-right pr-2">Serviços</th>
                                                </tr>
                                              : <tr>  
                                                    <th>Nome</th>
                                                    <th>E-Mail</th>  
                                                </tr>
                                            }
                                        </thead>
                                        <tbody>
                                            {detalhes}
                                        </tbody>
                                    </Table>
                                    { this.props.role !== ''
                                      ? <div>
                                            <Button type="button" value="primary" onClick={this.showDialog.bind(this)}
                                                disabled={this.state.ids.length === 0 || this.state.isSubmitting} 
                                                className="shadow border border-light">
                                                Remover os Cliente Selecionados</Button>
                                            <Link className="btn btn-secondary shadow ml-4" to="/" 
                                                disabled={this.state.isSubmitting}>Voltar</Link>
                                        </div>
                                      : <div>
                                            <Link className="btn btn-primary shadow ml-4" to="/" 
                                                disabled={this.state.isSubmitting}>Voltar</Link>
                                        </div>  
                                    }
                                </div>
                                <div hidden={this.state.clientes.length > 0}>
                                    <h5>Não existem <em>Clientes</em> cadastrados.</h5>
                                    <div>
                                        <Link className="btn btn-primary shadow" to="/">Voltar</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </Form>
                </Col>
            </div>
        );
    }
}

class Detalhe extends Component {
    state = {
        desativado: this.props.desativado
    }

    render() {
        return (
            this.props.role !== ''
            ? <tr>
                <td>
                    <Form.Check 
                        name="del"
                        checked={this.state.desativado}
                        value={this.props.idCliente} 
                        onChange={(event) => {
                            const state = !this.state.desativado;
                            this.setState({ desativado: state });
                            this.props.onChange();
                        }}/> 
                </td>
                <td>
                    <Link to={'cadCliente/' + this.props.idCliente}>
                        <span className="btn btn-light">
                            <FontAwesomeIcon icon={faPen} />
                        </span>
                    </Link>
                </td>
                <td>{this.props.nome}</td>
                <td>{this.props.email}</td>
                <td>
                    <div className="float-right pr-2">
                        <span className="rounded-circle p-2 bg-light shadow"
                            onClick={this.props.onClick}>
                            <FontAwesomeIcon icon={faPlus} />
                        </span>
                    </div>
                </td>
              </tr>
            : <tr>
                <td>{this.props.nome}</td>
                <td>{this.props.email}</td>
              </tr> 
            );
    }
}

const mapStateToProps = state => {
    return {
        role: state.role
    };
};

export default connect(mapStateToProps)(ListaCliente);