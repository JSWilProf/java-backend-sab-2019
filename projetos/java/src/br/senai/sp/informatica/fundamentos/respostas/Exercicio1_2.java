package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio1_2 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o Salário Hora");
		double salHora = Double.parseDouble(temp);
		String msg = String.format("Sal. Hora R$ %,.2f\n", salHora);
		
		temp = JOptionPane.showInputDialog("Informe a Qtd de Horas trabalhadas");
		int horas = Integer.parseInt(temp);
		msg += "Nº de Horas: " + horas + "\n" ;
		
		temp = JOptionPane.showInputDialog("Informe nº de Dependentes");
		int dep = Integer.parseInt(temp);
		msg += "Nº de Dependentes: " + dep + "\n";
		
		double salBruto = horas * salHora + 50 * dep;
		msg += String.format("Sal. Bruto R$ %,.2f\n", salBruto);
		
		double inss;
		if(salBruto <= 1000) {
			inss = salBruto * (8.5 / 100);
		} else {
			inss = salBruto * (9d / 100);
		}
		msg += String.format("Valor do INSS R$ %,.2f\n", inss);
		
		double ir;
		if(salBruto <= 500) {
			ir = 0;
		} else if(salBruto <= 1000) {
			ir = salBruto * (5d / 100);
		} else {
			ir = salBruto * (7d / 100);
		}
		msg += String.format("Valor do IR R$ %,.2f\n", ir);
		
		double salLiq = salBruto - inss - ir;
		msg += String.format("Sal. Liq. R$ %,.2f", salLiq);
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
