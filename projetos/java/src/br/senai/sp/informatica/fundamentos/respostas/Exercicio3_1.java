package br.senai.sp.informatica.fundamentos.respostas;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Exercicio3_1 {
	public static void main(String[] args) {
		String txt = "";
		int total = 1;
		for (int i = 1; i < 16; i++) {
			if(i % 2 != 0) {
				txt += i + " x ";
				total *= i;
			}
		}
		escreva(txt, "\nTotal: ", total);
	}
}
