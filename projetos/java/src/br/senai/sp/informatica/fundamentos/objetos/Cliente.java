package br.senai.sp.informatica.fundamentos.objetos;

import lombok.Data;

@Data
public class Cliente {
	private String nome;
	private String cpf;
	private String email;
	private String endereco;
	
	@Override
	public String toString() {
		return "";
	}
}
