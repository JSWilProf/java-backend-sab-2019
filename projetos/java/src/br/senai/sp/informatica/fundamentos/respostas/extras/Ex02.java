package br.senai.sp.informatica.fundamentos.respostas.extras;

import javax.swing.JOptionPane;

public class Ex02 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o 1º nota");
		int nota1 = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 2º nota");
		int nota2 = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 3º nota");
		int nota3 = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 4º nota");
		int nota4 = Integer.parseInt(temp);
		
		double media = (nota1 + nota2 + nota3 + nota4) / 4;
		
		JOptionPane.showMessageDialog(null, String.format("A média é: %,.2f", media));
	}
}
