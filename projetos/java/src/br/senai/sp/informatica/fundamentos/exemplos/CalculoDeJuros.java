package br.senai.sp.informatica.fundamentos.exemplos;

import javax.swing.JOptionPane;

public class CalculoDeJuros {
	public static void main(String[] args) {
		String aux = JOptionPane.showInputDialog("Informe o Preço");
		double preco = Double.parseDouble(aux);
		
		aux = JOptionPane.showInputDialog("Informe a Taxa");
		double taxa = Double.parseDouble(aux);
		
		double valorFinal = preco * taxa;
		
		JOptionPane.showMessageDialog(null, 
				"O valor final é de " + valorFinal);
	}
}
