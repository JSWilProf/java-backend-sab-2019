package br.senai.sp.informatica.fundamentos.respostas;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Exercicio4_2 {
	public static void main(String[] args) {
		String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
				"Outubro", "Novembro", "Dezembro" };

		int mes = leInteiro("Informe o nome do mês");
		
		while (mes != 0) {
			String nomeMes = "";
			
			if(mes > 0 && mes <= meses.length)
			   nomeMes = meses[mes -1];
			else
			   nomeMes = "Inválido, informe novamente";

			escreva("O nome do mês é: ", nomeMes);

			mes = leInteiro("Informe o nome do mês");
		}
	}
}
