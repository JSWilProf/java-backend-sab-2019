package br.senai.sp.informatica.fundamentos.exemplos;

import javax.swing.JOptionPane;

public class CalculoDeJuros2 {
	public static void main(String[] args) {
		String aux = JOptionPane.showInputDialog("Informe o Preço");
		double preco = Double.parseDouble(aux);
		
		double taxa;
		
		if(preco > 5000) {
			taxa = .1;
		} else {
			taxa = .05;
		}
		System.out.println("A taxa do cálculo é: " + taxa);
		
		double valorFinal = preco * taxa;
		
		JOptionPane.showMessageDialog(null, 
				"O valor final é de " + valorFinal);
	}
}
