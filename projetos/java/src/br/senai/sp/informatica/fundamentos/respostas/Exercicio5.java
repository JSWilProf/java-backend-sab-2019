package br.senai.sp.informatica.fundamentos.respostas;

import static br.senai.sp.informatica.lib.InputUtil.*;

import java.util.Arrays;

public class Exercicio5 {
	public static void main(String[] args) {
		int[] numeros = new int[5];
		
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = leInteiro("Informe o ", i+1, "º nº");
		}
		
		Arrays.sort(numeros);
		
		String txt = "Nºs Ordenados\n\n";
		for (int i = numeros.length -1; i >= 0; i--) {
			txt += numeros[i] + " ";
		}
		escreva(txt);
	}
}
