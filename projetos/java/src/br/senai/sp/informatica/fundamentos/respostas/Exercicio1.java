package br.senai.sp.informatica.fundamentos.respostas;

import javax.swing.JOptionPane;

public class Exercicio1 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog(
				"Informe a Qtd de Horas trabalhadas");
		int horas = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o Salário Hora");
		double salHora = Double.parseDouble(temp);
		
		temp = JOptionPane.showInputDialog("Informe nº de Dependentes");
		int dep = Integer.parseInt(temp);
		
		double salBruto = horas * salHora + 50 * dep;
		
		double inss;
		if(salBruto <= 1000) {
			inss = salBruto * (8.5 / 100);
		} else {
			inss = salBruto * (9d / 100);
		}
		
		double ir;
		if(salBruto <= 500) {
			ir = 0;
		} else if(salBruto <= 1000) {
			ir = salBruto * (5d / 100);
		} else {
			ir = salBruto * (7d / 100);
		}
		
		double salLiq = salBruto - inss - ir;
		
		JOptionPane.showMessageDialog(null,
			String.format("Sal. Hora R$ %,.2f\n", salHora) +
			"Nº de Horas: " + horas + "\n" +
			"Nº de Dependentes: " + dep + "\n" +
			String.format("Sal. Bruto R$ %,.2f\n", salBruto) +
			String.format("Valor do INSS R$ %,.2f\n", inss) +
			String.format("Valor do IR R$ %,.2f\n", ir) +
			String.format("Sal. Liq. R$ %,.2f", salLiq)
		);
	}
}
