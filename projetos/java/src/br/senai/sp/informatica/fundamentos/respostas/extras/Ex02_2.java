package br.senai.sp.informatica.fundamentos.respostas.extras;

import javax.swing.JOptionPane;

public class Ex02_2 {
	public static void main(String[] args) {
		double media = 0;

		for (int i = 1; i <= 4; i++) {
			String temp = JOptionPane.showInputDialog("Informe o "+ i +"º nota");
			media += Integer.parseInt(temp);
		}
				
		JOptionPane.showMessageDialog(null, String.format("A média é: %,.2f", media /= 4));
	}
}
