package br.senai.sp.informatica.fundamentos.exemplos;

import static br.senai.sp.informatica.lib.InputUtil.escreva;
import static br.senai.sp.informatica.lib.InputUtil.leInteiro;
import static br.senai.sp.informatica.lib.InputUtil.leReal;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Data;

public class AplicaTaxa {
	
	public static void main(String[] args) {
		int quantidade =  leInteiro("Informe a Quantidade de valores");
		
		escreva("O Total de valores Calculado: ",
				Stream.generate(() -> new ValorTaxa(leReal("Informe o Valor"), leReal("Informe a Taxa")))
					.limit(quantidade)
					.map(obj -> obj.calcula())
					.reduce(0d, (total, obj) -> total + obj));
	}	
}

@Data
@AllArgsConstructor
class ValorTaxa {
	private double valor;
	private double taxa;
	
	public double calcula() {
		return valor * taxa + valor;
	}
}