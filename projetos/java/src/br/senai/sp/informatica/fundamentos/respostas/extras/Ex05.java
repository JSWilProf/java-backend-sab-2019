package br.senai.sp.informatica.fundamentos.respostas.extras;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Ex05 {
	public static void main(String[] args) {
		int num = leInteiro("Informe um nº");
		 
		if(num % 2 == 0) escreva("É par");
		else escreva("É impar");
	}
}
