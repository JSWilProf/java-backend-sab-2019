package br.senai.sp.informatica.fundamentos.respostas.extras;

import static br.senai.sp.informatica.lib.InputUtil.*;

public class Ex04_2 {
	public static void main(String[] args) {
		escreva(
			leInteiro("Informe o 1º nº") == leInteiro("Informe o 2º nº")
			  ? "São iguais" 
			  : "São diferentes"
		);
	}
}
