package br.senai.sp.informatica.cadastro.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
 * CORS - Cross-Origin Resource Sharing 
 * (Compartilhamento de recursos com origens diferentes) 
 * é um mecanismo que usa cabeçalhos adicionais HTTP 
 * para informar a um navegador que permita que um 
 * aplicativo Web seja executado em uma origem (domínio) 
 * com permissão para acessar recursos selecionados de 
 * um servidor em uma origem distinta. Um aplicativo 
 * Web executa uma requisição cross-origin HTTP ao 
 * solicitar um recurso que tenha uma origem diferente 
 * (domínio, protocolo e porta) da sua própria origem.
 */

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	private final long MAX_AGE_SECS = 3600;

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
			.allowedOrigins("*")
			.allowedMethods("HEAD", "OPTIONS",  "PATCH", "GET", "PUT", "POST", "DELETE")
			.maxAge(MAX_AGE_SECS);
	}
}
